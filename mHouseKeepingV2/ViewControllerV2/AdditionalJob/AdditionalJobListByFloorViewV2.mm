//
//  AdditionalJobListByFloorViewV2.m
//  mHouseKeeping
//

#import "AdditionalJobListByFloorViewV2.h"
#import "FindByRoomCellV2.h"
#import "RoomManagerV2.h"
#import "FindByRoomFloorCellV2.h"
#import "NetworkCheck.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "AdditionalJobSearchViewV2.h"
#import "AdditionalJobManagerV2.h"
#import "FloorModelV2.h"
#import "QRCodeReader.h"
#import "LanguageManagerV2.h"
#import "AdditionalJobByRoomViewV2.h"
#import "AdditionalJobListScreenV2.h"
#import "DeviceManager.h"
#import "ehkDefinesV2.h"
#import "UIImage+Tint.h"
#import "STEncryptorDES.h"

@interface AdditionalJobListByFloorViewV2(PrivateMethods)

-(void) btnDetailArrowPressed:(NSInteger)index;
-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) btnBackPressed;
-(void) addBackButton;
- (void)addAccessoryButton:(UITableViewCell *)cell  andIndexPath:(NSIndexPath *)indexPath;

@end
@implementation AdditionalJobListByFloorViewV2


@synthesize tbvContent, listDisplayData, searchBar, searchView, isReloadDataOnShow;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //This will trig at search bar pressed
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //Remove notification for search bar
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardDidShow) name: UIKeyboardWillShowNotification object:nil];
    
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfAdditionalJob];
    
    [self loadAccessRights];
    
    if(!ENABLE_QRCODESCANNER)
    {
        if(ENABLE_CHECK_ACCESS_RIGHT_QRCODESCANNER)
        {
            [qrScanButton setHidden:NO];
            [qrScanButton setAlpha:0.7f];
            [qrScanButton setEnabled:NO];
            
            if (QRCodeScanner.isActive) {
                [qrScanButton setAlpha:1.0f];
                [qrScanButton setEnabled:YES];
            }
            
        }
        else
        {
            [qrScanButton setHidden:YES];
        }
    }
    
    //Delete old data of additional job
    if(!isDemoMode){
        int countDeleteData = [addJobManager deleteAllAddJobDataBeforeToday];
        if(countDeleteData > 0){
            isReloadDataOnShow = YES;
        }
    }
    
    // Load Data
    if(isReloadDataOnShow){
        isReloadDataOnShow = NO;
        [self performSelector:@selector(loadData) withObject:nil afterDelay:0.0f];
    }
    //Hao Tran - 2/26/2015 - Disable qrcode button in demo mode
    if (isDemoMode) {
        [qrScanButton setEnabled:NO];
    }
    
    [self loadFlatResource];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    isReloadDataOnShow = YES;
    listDisplayData = [NSMutableArray array];
    addJobManager = [AdditionalJobManagerV2 sharedAddionalJobManager];
    syncManager = [[SyncManagerV2 alloc] init];
    
    [tbvContent setBackgroundView:nil];
    [tbvContent setBackgroundColor:[UIColor clearColor]];
    [tbvContent setOpaque:YES];
    
    [self performSelector:@selector(loadTopbarView)];
    [self addQRButton];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark -
#pragma mark UITableView Editing rows

#pragma mark - UITableView Delegate Methods
-(CGFloat)tableView:(UITableView *)tableView 
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return heightTbvContent;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView 
    numberOfRowsInSection:(NSInteger)section {
    return [listDisplayData count];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor colorWithRed:colorWhite green:colorWhite 
                                            blue:colorWhite alpha:colorAlpha];
}

-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *IdentifyCell = @"IdentifyCell";
    FindByRoomFloorCellV2 *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:IdentifyCell];
    if(cell == nil){
        
        //Re-use nib name "FindByRoomFloorCellV2"
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FindByRoomFloorCellV2 class]) owner:self options:nil];
        cell = [array objectAtIndex:0];
        
        UIColor *badgeFrameColor = nil;
        BOOL isShining = NO;
        if(SYSTEM_VERSION_LESS_THAN(@"7.0")){
            badgeFrameColor = [UIColor whiteColor];
            isShining = YES;
        } else {
            badgeFrameColor = [UIColor clearColor];
        }
        
        CustomBadge *customBadge = [CustomBadge customBadgeWithString:nil
                                                       withStringColor:[UIColor whiteColor]
                                                        withInsetColor:[UIColor redColor]
                                                        withBadgeFrame:YES
                                                   withBadgeFrameColor:badgeFrameColor
                                                             withScale:1.0
                                                           withShining:isShining];
        customBadge.tag = 982;
        [customBadge setFrame:CGRectMake(230 ,15, 27, 25)];
        [cell.contentView addSubview:customBadge];
        [customBadge setHidden:YES];
        
        //Rectangle for badge
        //customBadge1.badgeCornerRoundness = 0.1f;
    }
    
    //cell.floorLabel.text = [NSString stringWithFormat:@"%@ %@",[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_ios_unassign_floor], floorModel.floor_name];
    
    AddJobFloorModelV2 *curFloor = nil;
    if([listDisplayData count] > 0 && [listDisplayData count] - 1 >= indexPath.row){
        curFloor = [listDisplayData objectAtIndex:indexPath.row];
    }
    int countAddJob = 0;
    if(curFloor != nil){
        cell.floorLabel.text = [NSString stringWithFormat:@"%@", curFloor.addjob_floor_name];
        countAddJob = (int)curFloor.addjob_floor_number_of_job;
    }
    
    CustomBadge *customBadge = (CustomBadge*)[cell.contentView viewWithTag:982];
    CGRect frameBadge = customBadge.frame;
    if(countAddJob <= 0)
    {
        [customBadge setHidden:YES];
    }
    else
    {
        [customBadge setHidden:NO];
        if(countAddJob > 0 && countAddJob <= 99) {
            frameBadge.size.width = 27;
            [customBadge setFrame:frameBadge];
        }else if(countAddJob >= 100 && countAddJob <= 999) {
            frameBadge.size.width = 30;
            [customBadge setFrame:frameBadge];
        } else if(countAddJob > 999) {
            frameBadge.size.width = 33;
            [customBadge setFrame:frameBadge];
        }
        
        if(countAddJob > 999) {
            [customBadge setBadgeText:@"999+"];
        } else {
            [customBadge setBadgeText:[NSString stringWithFormat:@"%d",countAddJob]];
        }
    }
    
    [cell.contentView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageBeforeiOS7:imgBgBigBtn equaliOS7:imgBgBigBtnFlat]]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    
    return cell;    
}

- (void)addAccessoryButton:(UITableViewCell *)cell  andIndexPath:(NSIndexPath *)indexPath{
    /*****************************************/
    UIButton *accessoryButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [accessoryButton setBackgroundColor:[UIColor clearColor]];
    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"] forState:UIControlStateNormal];
    [accessoryButton setTag:indexPath.row];
    
    [accessoryButton addTarget:self action:@selector(btnDetailPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell setAccessoryView:accessoryButton];
    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    /*****************************************/
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AddJobFloorModelV2 *curFloor = [listDisplayData objectAtIndex:indexPath.row];
    AdditionalJobByRoomViewV2 *roomListView = [[AdditionalJobByRoomViewV2 alloc] initWithNibName:@"AdditionalJobByRoomViewV2" bundle:nil];
    roomListView.floorId = curFloor.addjob_floor_id;
    if([listItemsSearch count] > 0) {
        roomListView.listSearchAddJobItems = listItemsSearch;
    }
    [self.navigationController pushViewController:roomListView animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
}

#pragma mark - UISearchBar Delegate Methods

-(void)keyboardDidShow
{
    [self beginSearch];
}

-(void)beginSearch
{
    if(!hasCancelSearch)
    {
        [searchBar resignFirstResponder];
        [self navigateToSearchScreen];
    }
    else
    {
        [searchBar resignFirstResponder];
        hasCancelSearch = NO;
    }
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)currentSearchBar {
    [self beginSearch];
}

-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

-(void)searchBar:(UISearchBar *)currentSearchBar textDidChange:(NSString *)searchText{
    [searchBar resignFirstResponder];
    [searchBar setText:nil];
    if(searchText.length <= 0){
        [listItemsSearch removeAllObjects];
        hasCancelSearch = YES;
        [searchBar setPlaceholder:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SEARCH]];
        /*
        UILabel *labelSearch = [self findSearchLabelInSearchBar];
        [labelSearch setText:@""];
        */
        
        //Show loading indicator and reload all floor data
        MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
        [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
        [self.tabBarController.view addSubview:HUD];
        [HUD show:YES];
        [[HomeViewV2 shareHomeView] waitingLoadingData];
        
        [self loadDataWithSearch:NO];
        [tbvContent reloadData];
        
        [HUD removeFromSuperview];
        [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)currentSearchBar {
    [self beginSearch];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)currentSearchBar
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {

    //clear text
    return YES;
}

-(void)navigateToSearchScreen
{
    AdditionalJobSearchViewV2 *addJobSearchView = [[AdditionalJobSearchViewV2 alloc] initWithNibName:@"AdditionalJobSearchViewV2" bundle:nil];
    addJobSearchView.delegate = self;
    if([listItemsSearch count] > 0) {
        addJobSearchView.listAddJobsSelected = [NSMutableArray arrayWithArray:listItemsSearch];
    }
    [self.navigationController pushViewController:addJobSearchView animated:YES];
}

#pragma mark - Additional Job Delegate

-(UILabel*)findSearchLabelInSearchBar
{
    //Find text field in search bar
    UITextField *tf = nil;
    for (UIView *view in searchBar.subviews){
        if ([view isKindOfClass: [UITextField class]]) {
            tf = (UITextField *)view;
            break;
        }
    }
    
    //Find Label in text field
    UILabel *labelSearch = nil;
    for (UIView *curView in tf.subviews) {
        if ([curView isKindOfClass: [MarqueeLabel class]]) {
            labelSearch = (MarqueeLabel *)curView;
            break;
        }
    }
    return  labelSearch;
}

-(void)didSelectAdditionalJobs:(NSMutableArray*)listAddJobsSelected
{
    NSMutableString *stringSearch = [[NSMutableString alloc] initWithString:@""];
    bool isEnglishLang = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE];
    for (int i = 0; i < [listAddJobsSelected count]; i++) {
        
        AddJobItemModelV2 *curAddJob = [listAddJobsSelected objectAtIndex:i];
        if(isEnglishLang){
            [stringSearch appendString:curAddJob.addjob_item_name];
        } else {
            [stringSearch appendString:curAddJob.addjob_item_name_lang];
        }
        
        if(i < [listAddJobsSelected count] - 1) {
            [stringSearch appendString:@", "];
        }
    }
    /*
    UILabel *labelSearch = [self findSearchLabelInSearchBar];
    if(labelSearch == nil)
    {
        //Find text field in search bar
        UITextField *tf = nil;
        for (UIView *view in searchBar.subviews){
            if ([view isKindOfClass: [UITextField class]]) {
                tf = (UITextField *)view;
                break;
            }
        }
        MarqueeLabel *marqueeLabelSearch = [[MarqueeLabel alloc] initWithFrame:CGRectMake(tf.frame.origin.x + 25, 0, tf.frame.size.width - 55, tf.frame.size.height)];
        [marqueeLabelSearch setBackgroundColor:[UIColor clearColor]];
        [marqueeLabelSearch setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
        [marqueeLabelSearch setTextColor:[UIColor lightGrayColor]];
        marqueeLabelSearch.textAlignment = NSTextAlignmentLeft;
        marqueeLabelSearch.marqueeType = MLContinuous;
        marqueeLabelSearch.continuousMarqueeExtraBuffer = 80.0f;
        marqueeLabelSearch.animationDelay = 0;
        
        [labelSearch removeFromSuperview];
        labelSearch = marqueeLabelSearch;
        
        [tf addSubview:labelSearch];
        
    }
     */
    
    //Did choose addJobData
    //[labelSearch setText:stringSearch];
    //[searchBar setText:@"  "]; //to show signal (x) in search bar
    [searchBar setText:stringSearch];
    [searchBar setPlaceholder:@""];
    
    //list addJob selected
    listItemsSearch = listAddJobsSelected;
    
    // Load Data
    [self performSelector:@selector(searchFloors) withObject:nil afterDelay:0.0f];
    
}

-(void) searchFloors
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    [self loadDataWithSearch:YES];
    [tbvContent reloadData];
    
    [HUD removeFromSuperview];
    //[self hiddenHUDAfterSaved:HUD];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - Load Data
-(void) loadAccessRights
{
    AccessRightModel *currentUserAccessRight = [[UserManagerV2 sharedUserManager] currentUserAccessRight];
    QRCodeScanner = [[CommonVariable sharedCommonVariable] parse:currentUserAccessRight.QRCodeScanner];
    
}

-(int) getBuildingIdByUserId:(int) userId
{
    /*
    // get roomAssignment and get buildingId from roomassignment
    RoomAssignmentModelV2 *roomAssignment = [[RoomAssignmentModelV2 alloc] init];
    roomAssignment.roomAssignment_UserId = [UserManagerV2 sharedUserManager].currentUser.userId;
    
    NSMutableArray *roomAssignmentList = [[RoomManagerV2 sharedRoomManager] loadAllRoomAssignmentsByUserID:roomAssignment];
    if([roomAssignmentList count] > 0){
        roomAssignment = (RoomAssignmentModelV2*)[roomAssignmentList objectAtIndex:0];
    }
    
    RoomModelV2 *roomModel = [[RoomModelV2 alloc] init];
    roomModel.room_Id = roomAssignment.roomAssignment_RoomId;
    [[RoomManagerV2 sharedRoomManager] loadRoomModel:roomModel];
    
    //Hao Tran - Hard code building id if BuildingId doesn't has data
    if(roomModel.room_Building_Id <= 0){
        return 1;
    }

    return roomModel.room_Building_Id;
     */
    NSInteger buildingId = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"%@%d", CURRENT_BUILDING_ID, [UserManagerV2 sharedUserManager].currentUser.userId]];
    if(buildingId <= 0) {
        return 1;
    }
    
    return (int)buildingId;
}

-(int)countPendingJobsWith:(AddJobFloorModelV2*) addJobFloorModel andListCompare: (NSMutableSet*) listCompare{
    int countPendingJobs = 0;
    NSMutableArray *listAddJobRooms = [addJobManager loadAddJobRoomsByUserId:[UserManagerV2 sharedUserManager].currentUser.userId floorId:(int)addJobFloorModel.addjob_floor_id searchId:0];
    for(NSString *roomNumber in listCompare){
        for(AddJobRoomModelV2 *addJobRoomModel in listAddJobRooms){
            if([addJobRoomModel.addjob_room_number isEqualToString:roomNumber]){
                countPendingJobs += addJobRoomModel.addjob_room_number_pending_job;
            }
        }
    }
    
    return countPendingJobs;
}

-(void)loadDataWithSearch:(BOOL) isSearching
{
    [listDisplayData removeAllObjects];
    
    if(isSearching)
    {
        NSMutableArray * listSearchItems = [NSMutableArray array];
        for (AddJobItemModelV2 *curItem in listItemsSearch) {
            [listSearchItems addObject:[NSNumber numberWithInteger:curItem.addjob_item_id]];
        }
        bool isSearchSuccess = NO;
        
        if(isDemoMode){
            int searchId;
            
            //Insert into addjob_search
            AddJobSearchModelV2 *searchModel = [addJobManager loadAddJobSearchByUserId:[UserManagerV2 sharedUserManager].currentUser.userId tasksFilter:listSearchItems];
            if(searchModel){
                searchId = (int)searchModel.addjob_search_id;
            } else {
                AddJobSearchModelV2 *searchModel = [[AddJobSearchModelV2 alloc] init];
                NSString *todayDateString = [ehkConvert getTodayDateString];
                searchModel.addjob_search_last_modified = todayDateString;
                NSString *taskListString = nil;
                if([listSearchItems count] > 0){
                    taskListString = [addJobManager convertListTasksIdToString:listSearchItems];
                }
                searchModel.addjob_search_tasks_id = taskListString;
                searchModel.addjob_search_user_id = [UserManagerV2 sharedUserManager].currentUser.userId;
                searchId = [addJobManager insertAddJobSearch:searchModel];
            }
            
            listDisplayData = [addJobManager loadAllAddJobFloorsByUserId:[UserManagerV2 sharedUserManager].currentUser.userId searchId: searchId];
            
            //Insert into addjob_floor
            if([listDisplayData count] == 0){
                listDisplayData = [[NSMutableArray alloc] init];
                NSMutableArray *listTempAddJobFloor = [[NSMutableArray alloc] init];
                NSMutableArray *listTempAddJobRoom = [[NSMutableArray alloc] init];
                NSMutableSet *listAddJobFloorNumber = [[NSMutableSet alloc] init];
                NSMutableSet *listAddJobRoomNumber = [[NSMutableSet alloc] init];
                
                int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
                for(NSNumber *itemId in listSearchItems){
                    NSMutableArray *listAddJobRoomItem = [addJobManager loadListAddJobRoomItemByUserId:userId itemId:[itemId intValue]];
                    for(AddJobRoomItemModelV2 *addJobRoomItemModel in listAddJobRoomItem){
                        AddJobDetailModelV2 *addJobDetail = [addJobManager loadAddJobDetailByUserId:userId ondayAddJobId:(int)addJobRoomItemModel.addjob_roomitem_onday_addjob_id];
                        AddJobRoomModelV2 *addJobRoomModel = [addJobManager loadAddJobRoomByRoomNumber:(int)addJobDetail.addjob_detail_room_id searchId:0];
                        if(addJobRoomModel != nil){
                            addJobRoomModel.addjob_room_search_id = searchId;
                            [listTempAddJobRoom addObject:addJobRoomModel];
                            //----------------------------------------------//
                            AddJobFloorModelV2 *addJobFloorModel = [addJobManager loadAddJobFloorByUserId:userId floorId:(int)addJobRoomModel.addjob_room_floor_id searchId:0];
                            addJobFloorModel.addjob_floor_search_id = searchId;
                            [listTempAddJobFloor addObject:addJobFloorModel];
                        }
                    }
                }
                
                //Remove duplicate Addjob Rooms
                if([listTempAddJobRoom count] != 0){
                    for(AddJobRoomModelV2 *model in listTempAddJobRoom){
                        if(![listAddJobRoomNumber containsObject:model.addjob_room_number]){
                            [addJobManager insertAddJobRoom:model];
                        }
                        [listAddJobRoomNumber addObject:model.addjob_room_number];
                    }
                }
                
                //Remove duplicate Addjob Floors
                if([listTempAddJobFloor count] != 0){
                    for(AddJobFloorModelV2 *model in listTempAddJobFloor){
                        
                        if(![listAddJobFloorNumber containsObject:model.addjob_floor_name]){
                            //Count pending jobs
                            model.addjob_floor_number_of_job = [self countPendingJobsWith:model andListCompare:listAddJobRoomNumber];
                            [listDisplayData addObject:model];
                            [addJobManager insertAddJobFloor:model];
                        }
                        [listAddJobFloorNumber addObject:model.addjob_floor_name];
                    }
                }
            }
            
            //---------------------------------------------------------------------//
            isSearchSuccess = YES;
        }
        else{
            isSearchSuccess = [addJobManager loadWSAddJobFloorListByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId tasksFilter:listSearchItems];
            if(isSearchSuccess){
                AddJobSearchModelV2 *searchSelected = [addJobManager loadAddJobSearchByUserId:[UserManagerV2 sharedUserManager].currentUser.userId tasksFilter:listSearchItems];
                int searchId = (int)searchSelected.addjob_search_id;
                listDisplayData = [addJobManager loadAllAddJobFloorsByUserId:[UserManagerV2 sharedUserManager].currentUser.userId searchId: searchId];
            }
        }
    }
    else
    {
        if(!isDemoMode){
            [addJobManager loadWSAddJobFloorListByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId tasksFilter:nil];
            listDisplayData = [addJobManager loadAllAddJobFloorsByUserId:[UserManagerV2 sharedUserManager].currentUser.userId searchId:0];
        }
        else{
            listDisplayData = [addJobManager loadAllAddJobFloorsByUserId:[UserManagerV2 sharedUserManager].currentUser.userId searchId:0];
        }
    }
    
    if (listDisplayData.count == 0) {
//        lblNoResult  =  [[UILabel alloc]init];
//        lblNoResult.frame     =  CGRectMake(15, 150, 290, 80);
//        [lblNoResult setText:[[LanguageManagerV2 sharedLanguageManager] getNoResultsFound]];
//        lblNoResult.textAlignment = NSTextAlignmentCenter;
//        lblNoResult.layer.cornerRadius = 10;
//        [lblNoResult setFont:[UIFont fontWithName:@"Arial-BoldMT" size:22]];
//        lblNoResult.numberOfLines = 2;
//        [lblNoResult setTextColor:[UIColor grayColor]];
//        //lblAlert.font = [UIFont boldSystemFontOfSize:20.0f];
        [tbvContent setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//        [tbvContent addSubview:lblNoResult];
//    }
//    else{
//        if(lblNoResult){
//            [lblNoResult removeFromSuperview];
//        }
    }
}

-(void)loadData
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    if(searchBar.text.length > 0){
        [self loadDataWithSearch:YES];
    }
    else{
        [self loadDataWithSearch:NO];
    }
    
    //refresh badge number if current badge number is 0
    if([listDisplayData count] > 0){
        
        //Sync number of additional job floor with Badge Number
        int countAddJob = 0;
        for (int i = 0; i < [listDisplayData count]; i ++) {
            AddJobFloorModelV2 *curFloor = [listDisplayData objectAtIndex:i];
            countAddJob += curFloor.addjob_floor_number_of_job;
        }
        
        if([HomeViewV2 shareHomeView].countPendingAddJobs != countAddJob){
            [HomeViewV2 shareHomeView].countPendingAddJobs = countAddJob;
            [[HomeViewV2 shareHomeView] updateBadgeNumber:NO]; //parameter YES is load count pending additional job from WS
        }
    }
    
    [tbvContent reloadData];
    
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    
    //[self hiddenHUDAfterSaved:HUD];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - Action for Button Detail Arrow
-(void) btnDetailPressed:(UIButton *) sender {
    [self btnDetailArrowPressed:sender.tag];
}

#pragma mark - === Handle Topbar Methods ===
#pragma mark
-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
    
    //Set show topbar
    //[self showTopbarView];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    SuperRoomModelV2 *modelSuper = [[SuperRoomModelV2 alloc] init];
    NSString* propertyName = [[RoomManagerV2 sharedRoomManager] getPropertyName:modelSuper];
    NSString* propertyNameStr = [NSString stringWithFormat:@"%@",propertyName== nil ? @"":propertyName];
    
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, -5, 200, 30)];
    [titleBarButtonFirst setTag:1];
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleBarButtonFirst.titleLabel setAdjustsFontSizeToFitWidth:YES];
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:propertyNameStr forState:UIControlStateNormal];
    [vBarButton addSubview:titleBarButtonFirst];
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 23, 200, 22)];
    [titleBarButtonSecond setTag:2];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonSecond setTitle:[[RoomManagerV2 sharedRoomManager] getHouseKeeperName] forState:UIControlStateNormal];
    [vBarButton addSubview:titleBarButtonSecond];
    
    [self.navigationItem setTitleView:vBarButton];
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
}


-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.3];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = searchView.frame;
    [searchView setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height)];
    
    ftbv = tbvContent.frame;
    [tbvContent setFrame:CGRectMake(0, f.size.height + searchBar.frame.size.height, 320, ftbv.size.height - f.size.height)];
}

-(void)adjustRemoveForViews { 
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [searchView setFrame:CGRectMake(0, 0, searchView.frame.size.width, searchView.frame.size.height)];
    
    CGRect ftbv = tbvContent.frame;
    [tbvContent setFrame:CGRectMake(0, searchView.frame.size.height, tbvContent.frame.size.width, ftbv.size.height + f.size.height)];
}

#pragma mark - === Handle Back Button ===
#pragma mark

-(void)backBarPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) btnQRCodeScannerTouched:(id) sender
{
    ZXingWidgetController *widController = [[ZXingWidgetController alloc] initWithDelegate:self showCancel:YES OneDMode:NO showLicense:NO];
    QRCodeReader* qrcodeReader = [[QRCodeReader alloc] init];
    NSSet *readers = [[NSSet alloc ] initWithObjects:qrcodeReader,nil];
    widController.readers = readers;
    [self presentViewController:widController animated:YES completion:nil];
}

#pragma mark - ZxingDelegate
- (void)zxingController:(ZXingWidgetController*)controller didScanResult:(NSString *)resultString {
    resultString = [STEncryptorDES convertText:resultString];
    [self dismissViewControllerAnimated:YES completion:nil];
    BOOL isSuccess = YES;
    
    //Show loading indicator and reload all floor data
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    //Checking valid room number here
    isSuccess = [addJobManager loadWSAddJobRoomByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:resultString];
    if (!isSuccess) {
        NSString *alertContent = nil;
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            alertContent = [L_wrong_room currentKeyToLanguage];
        } else {
            alertContent = [NSString stringWithFormat:@"\n\n\n\n\n%@!", [L_wrong_room currentKeyToLanguage]];
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[L_alert currentKeyToLanguage] message:alertContent delegate:self cancelButtonTitle:[L_CLOSE currentKeyToLanguage] otherButtonTitles:nil];
        UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(80, 35, 120, 120)];
        [alertImage setImage:[UIImage imageNamed:@"icon_wrong room.png"]];
        [alert addSubview:alertImage];
        [alert show];
    } else {
        AddJobRoomModelV2 *roomSelected = [[AddJobRoomModelV2 alloc] init];
        roomSelected = [addJobManager loadAddJobRoomQRCodeByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomNumber:resultString];
        AdditionalJobListScreenV2 *addJobListScreen = [[AdditionalJobListScreenV2 alloc] initWithNibName:@"AdditionalJobListScreenV2" bundle:nil];
        addJobListScreen.roomSelected = roomSelected;
        [self.navigationController pushViewController:addJobListScreen animated:YES];
    }
    [HUD removeFromSuperview];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

- (void)zxingControllerDidCancel:(ZXingWidgetController*)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)addQRButton {
    
    qrScanButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *img = [UIImage imageNamed:@"icon_QR_code_2.png"];
    [qrScanButton setBackgroundImage:img forState:UIControlStateNormal];
    [qrScanButton addTarget:self action:@selector(btnQRCodeScannerTouched:) forControlEvents:UIControlEventTouchUpInside];
    [qrScanButton setFrame:CGRectMake(0, 0, 50, 50)];
    UIView *backButtonView = [[UIView alloc] initWithFrame:qrScanButton.frame];
    
    int moveLeftButton = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftButton = 10;
    }
    backButtonView.bounds = CGRectMake(backButtonView.bounds.origin.x + moveLeftButton, 5, backButtonView.bounds.size.width, backButtonView.bounds.size.height);
    [backButtonView addSubview:qrScanButton];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.leftBarButtonItem = barButtonItem;

}

-(void)loadFlatResource {
    [searchBar setPlaceholder:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SEARCH]];
}
@end
