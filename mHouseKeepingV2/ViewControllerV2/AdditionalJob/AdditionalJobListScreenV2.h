//
//  AdditionalJobByRoomViewV2.h
//  mHouseKeeping

#import <UIKit/UIKit.h>
#import "AdditionalJobManagerV2.h"
#import "RoomManagerV2.h"
#import "RoomAssignmentModelV2.h"
#import "GuestInfoViewV2.h"
#import "GuestInfoModelV2.h"

@interface AdditionalJobListScreenV2 : UIViewController 
    <UITableViewDelegate, UITableViewDataSource, RemarkViewV2Delegate, ActionPopupDelegate> {
    
        AdditionalJobManagerV2 *addJobManager;
        NSMutableArray *listDetailDisplay;
        
        IBOutlet UIScrollView *scrContent;
        IBOutlet UIScrollView *scrGuests;
        IBOutlet UIButton *btnAction;
        IBOutlet UIButton *btnGuestInfo;
        IBOutlet UIButton *btnRoomStatus;
        IBOutlet UIButton *btnRoomAssignment;
        IBOutlet UIImageView *imgRushQueueRoom;
        IBOutlet UIImageView *imgRoomStatus;
        IBOutlet UILabel *lblGuestInfo;
        IBOutlet UIView *viewAction;
        IBOutlet UITableView *tbvContent;
        IBOutlet UILabel *lblRoomStatus;
        
        RemarkViewV2 *remarkView;
        ActionPopupView *actionPopup;
        //GuestInfoViewV2 *guestInfo;
        //GuestInfoViewV2 *guestInfoArrival;
        NSMutableArray *listGuestModels;
        NSMutableArray *listGuestViews;
        UIImageView *guestIndicatorLeft;
        UIImageView *guestIndicatorRight;
        
        AddJobGuestInfoModelV2 *guestInfoModel;
        AddJobGuestInfoModelV2 *guestInfoArrivalModel;
        UIButton *titleBarButtonFirst;
        BOOL isAlreadyLayoutSubviewsFirstLoad;
        bool isAlreadyGetGuest;
        UIButton *titleBarButtonSecond;
}
@property (nonatomic) AddJobRoomModelV2 *roomSelected;

-(void) loadData;
-(void) setCaptionsView;

-(IBAction)btnGuestInfoClicked:(id)sender;
-(IBAction)btnRoomAssignmentInfo:(id)sender;

@end
