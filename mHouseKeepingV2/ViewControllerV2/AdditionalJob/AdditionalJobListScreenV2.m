//
//  AdditionalJobByRoomViewV2.m
//  mHouseKeeping
//

#import "AdditionalJobListScreenV2.h"
#import "AdditionalJobRoomCellV2.h"
#import "RoomManagerV2.h"
#import "RoomModelV2.h"
#import "FloorModelV2.h"
#import "AddJobDetailModelV2.h"
#import "AdditionalJobManagerV2.h"
#import "MyNavigationBarV2.h"
#import "LogFileManager.h"
#import "UnassignManagerV2.h"
#import "DeviceManager.h"
#import "AdditionalJobDetailsScreenV2.h"
#import "GuidelineViewV2.h"
#import "iToast.h"
#import "UIImage+Tint.h"
#import "ehkDefinesV2.h"
#import "CustomAlertViewV2.h"
#import "AdditionalJobDetailCell.h"

#define markImage           @"!.png"
#define backgroundImage     @"bg.png"

#define heightRemarkCell    60.0f
#define heightAddJobCell    45.0f
#define heightHeaderTable   20.0f
#define heightFooterTable   4.0f
#define tagNoResult         12345
#define numberTableSection  2

#define heightTbvNoResult   240.0f
#define sizeNoResult        22.0f
#define sizeNormal          17.0f
#define sizeSmall           10.0f

#define colorAlpha          1.0
#define colorWhite          255/255.0
#define colorBlueR          105/255.0
#define colorBlueG          218/255.0
#define colorBlueB          222/255.0

//Local Model for self cell display
@interface DetailsDisplay : NSObject
@property (nonatomic, retain) AddJobDetailModelV2 *addJobDetail;
@property (nonatomic, retain) NSString *addJobCategoryString;
@end

@implementation DetailsDisplay
@synthesize addJobDetail,addJobCategoryString;
@end

@interface AdditionalJobListScreenV2(PrivateMethods)

-(void) btnDetailArrowPressed:(NSInteger)index;

-(void) loadTopbarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) setHiddenTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void) addButtonHandleShowHideTopbar;
-(void) topbarTapped:(UIButton *) sender;
-(TopbarViewV2 *) getTopBarView;
-(void) btnBackPressed;
-(void) addBackButton;
- (void)addAccessoryButton:(UITableViewCell *)cell  andIndexPath:(NSIndexPath *)indexPath;

@end

@implementation AdditionalJobListScreenV2
@synthesize roomSelected;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    //Fix bug content in table is lost apart on first load
    if(!isAlreadyLayoutSubviewsFirstLoad){
        isAlreadyLayoutSubviewsFirstLoad = YES;
        TopbarViewV2 *topbar = [self getTopBarView];
        BOOL isTopbarShowing = (topbar.isHidden != YES || topbar.alpha > 0.0f);
        
        [self modifyAllViews:isTopbarShowing];
    }
}


-(void)viewWillAppear:(BOOL)animated {
    [self setCaptionsView];
    [super viewWillAppear:animated];
    
    //set selection find in bottom bar
    [[HomeViewV2 shareHomeView] setSelectedButton:tagOfAdditionalJob];
    
    // Load Data
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.0f];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    TopbarViewV2 *topbar = [self getTopBarView];
    BOOL isTopbarShowing = (topbar.isHidden != YES || topbar.alpha > 0.0f);
    [self modifyAllViews:isTopbarShowing];
    [self populateData];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [self loadFlatResource];
    }
    isAlreadyLayoutSubviewsFirstLoad = NO;
    isAlreadyGetGuest = NO;
    addJobManager = [AdditionalJobManagerV2 sharedAddionalJobManager];
    listDetailDisplay = [NSMutableArray array];
    
    [tbvContent setBackgroundView:nil];
    [tbvContent setBackgroundColor:[UIColor clearColor]];
    [tbvContent setOpaque:YES];
    
    [self performSelector:@selector(loadTopbarView)];
    
    [self addBackButton];
    
}



- (void)viewDidUnload {
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - UITableView Delegate Methods

-(int)sectionAddJob
{
    return 0;
}
-(int)sectionRemark
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceScreenSizeStandardWidth3_5, 45)];
    NSString *fontType = @"Helvetica-Bold";
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, -12, DeviceScreenSizeStandardWidth3_5 - 30, 45)];
    [label setTextColor:[UIColor colorWithRed:6.0f/255.0f green:62.0f/255.0f blue:127.0f/255.0f alpha:1]];
    [label setFont:[UIFont fontWithName:fontType size:13]];
    [label setBackgroundColor:[UIColor clearColor]];
    
    if(section == [self sectionAddJob]) {
        [label setText:[L_ADDITIONAL_JOB currentKeyToLanguage]];
    } else if(section == [self sectionRemark]) {
        [label setText:[L_remark currentKeyToLanguage]];
    }
    
    [headerView addSubview:label];
    return headerView;
    
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 4.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

-(CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == [self sectionAddJob])
    {
        return heightAddJobCell;
    }
    else //Remark section
    {
        return heightRemarkCell;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;   //Hide Remark
}

-(NSInteger)tableView:(UITableView *)tableView
numberOfRowsInSection:(NSInteger)section {
    if(section == [self sectionAddJob]){
        return [listDetailDisplay count];
    } else { //Section remark
        return 1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellAddJobIdentify = @"AddJobCellIdentify";
    static NSString *cellRemarkIdentify = @"RemarkCellIdentify";
    UITableViewCell *cell = nil;
    
    //create cell
    if(indexPath.section == [self sectionAddJob])
    {
        cell = [tableView dequeueReusableCellWithIdentifier:cellAddJobIdentify];
        if (cell == nil) {
            NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AdditionalJobDetailCell class]) owner:self options:nil];
            cell = [array objectAtIndex:0];
            [self addAccessoryButton:cell andIndexPath:indexPath];
        }
    }
    else //Remark
    {
        cell = [tableView dequeueReusableCellWithIdentifier:cellRemarkIdentify];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellSelectionStyleNone reuseIdentifier:cellRemarkIdentify];
            //[cell.textLabel setFrame:CGRectMake(15, 0, tableView.bounds.size.width - 35, 60)];
            //[cell.textLabel setTag:remarkTag];
            [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20]];
            [cell.textLabel setAdjustsFontSizeToFitWidth:NO];
            [cell.textLabel setNumberOfLines:2];
            [cell.textLabel setLineBreakMode:NSLineBreakByTruncatingTail];
            [cell.textLabel setBackgroundColor:[UIColor clearColor]];
            [cell.textLabel setTextColor:[UIColor grayColor]];
        }
    }
    
    if(indexPath.section == [self sectionAddJob])
    {
        DetailsDisplay *addJobDetailDisplay = [listDetailDisplay objectAtIndex:indexPath.row];
        AdditionalJobDetailCell *addJobCell = (AdditionalJobDetailCell*)cell;
        
        [addJobCell.lblAddJobTitle setText:addJobDetailDisplay.addJobCategoryString];
        if ([[UserManagerV2 sharedUserManager].currentUserAccessRight isUseAdditionalJobStatusTable]) {
            if (addJobDetailDisplay.addJobDetail.addjob_detail_status == AddJobStatus_DND) {
                [addJobCell.imgAddJobStatus setHidden:NO];
                [addJobCell.lblTime setHidden:YES];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                    [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgDNDFlat]];
                } else {
                    [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgDND]];
                }
                
            } else if (addJobDetailDisplay.addJobDetail.addjob_detail_status == AddJobStatus_DoubleLock) {
                [addJobCell.imgAddJobStatus setHidden:NO];
                [addJobCell.lblTime setHidden:YES];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                    [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgDoubleLockFlat]];
                } 
                
            } else if(addJobDetailDisplay.addJobDetail.addjob_detail_status == AddJobStatus_DeclinedService) {
                [addJobCell.imgAddJobStatus setHidden:NO];
                [addJobCell.lblTime setHidden:YES];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                    [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgDeclinedServiceFlat]];
                } else {
                    [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgDeclinedService]];
                }
                
            } else if(addJobDetailDisplay.addJobDetail.addjob_detail_status == AddJobStatus_ServiceLater) {
                [addJobCell.imgAddJobStatus setHidden:NO];
                [addJobCell.lblTime setHidden:NO];
                [addJobCell.lblTime setText:[ehkConvert DateToStringWithString:addJobDetailDisplay.addJobDetail.addjob_detail_assigned_date fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"HH:mm"]];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                    [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgServiceLaterFlat]];
                } else {
                    [addJobCell.imgAddJobStatus setImage:[UIImage imageNamed:imgServiceLater]];
                }
                
            } else if(addJobDetailDisplay.addJobDetail.addjob_detail_status == AddJobStatus_Complete){
                [addJobCell.imgAddJobStatus setHidden:YES];
                [addJobCell.lblTime setHidden:YES];
                UIImageView *indicatorView = [[UIImageView alloc] initWithImage:[UIImage imageBeforeiOS7:imgYesIcon equaliOS7:imgYesIconFlat]];
                [indicatorView setFrame:CGRectMake(0, 0, 26, 26)];
                [cell setAccessoryView:indicatorView];
                [cell setUserInteractionEnabled:NO];
            } else {
                [addJobCell.imgAddJobStatus setHidden:YES];
                [addJobCell.lblTime setHidden:YES];
            }
        } else {
            if(addJobDetailDisplay.addJobDetail.addjob_detail_status == 1){
                [addJobCell.imgAddJobStatus setHidden:YES];
                [addJobCell.lblTime setHidden:YES];
                UIImageView *indicatorView = [[UIImageView alloc] initWithImage:[UIImage imageBeforeiOS7:imgYesIcon equaliOS7:imgYesIconFlat]];
                [indicatorView setFrame:CGRectMake(0, 0, 26, 26)];
                [cell setAccessoryView:indicatorView];
                [cell setUserInteractionEnabled:NO];
            } else {
                [addJobCell.imgAddJobStatus setHidden:YES];
                [addJobCell.lblTime setHidden:YES];
            }
        }
    }
    else if(indexPath.section == [self sectionRemark])
    {
        [cell.textLabel setText:roomSelected.addjob_room_remark];
    }
    
    return cell;
    
}


- (void)addAccessoryButton:(UITableViewCell *)cell  andIndexPath:(NSIndexPath *)indexPath{
    /*****************************************/
    UIButton *accessoryButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [accessoryButton setBackgroundColor:[UIColor clearColor]];
    //    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"] forState:UIControlStateNormal];
    [accessoryButton setImage:[UIImage imageNamed:@"gray_arrow_36x36.png"] forState:UIControlStateNormal];
    [accessoryButton setTag:indexPath.row];
    
    //[accessoryButton addTarget:self action:@selector(btnDetailPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell setAccessoryView:accessoryButton];
    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    /*****************************************/
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelected:NO];
    
    //Checking Access Right - Additional job is only allow view
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isAddJobAllowedView]
       || ![[UserManagerV2 sharedUserManager].currentUserAccessRight isAddJobActive]) {
        
        [[[[iToast makeText:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_access_right_access_denied]] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
        return;
    }
    
    if(indexPath.section == [self sectionAddJob]) {
        int rowSelected = (int)indexPath.row;
        DetailsDisplay *addJobDetailDisplay = [listDetailDisplay objectAtIndex:rowSelected];
        AdditionalJobDetailsScreenV2 *addJobDetailView = [[AdditionalJobDetailsScreenV2 alloc] initWithNibName:@"AdditionalJobDetailsScreenV2" bundle:nil];
        addJobDetailView.detailSelected = addJobDetailDisplay.addJobDetail;
        addJobDetailView.roomSelected = roomSelected;
        addJobDetailView.isPushFrom = IS_PUSHED_FROM_ADD_JOB_LIST;
        [self.navigationController pushViewController:addJobDetailView animated:YES];
    }
    else if(indexPath.section == [self sectionRemark])
    {
        if (remarkView == nil) {
            remarkView = [[RemarkViewV2 alloc] initWithNibName:@"RemarkViewV2" bundle:nil];
        }
        UIView *view = remarkView.view;
        [remarkView setDelegate:self];
        [remarkView setTextinRemark:roomSelected.addjob_room_remark != nil ? roomSelected.addjob_room_remark : @""];
        [remarkView viewWillAppear:NO];
        remarkView.isAddSubview = YES;
        [self.tabBarController.view addSubview:view];
    }
}

-(int)totalHeightOfTableContent
{
    int total = heightAddJobCell * [listDetailDisplay count];
    total += heightRemarkCell;
    total += heightFooterTable * numberTableSection;
    total += heightHeaderTable * numberTableSection;
    total += 25;
    
    return total;
}

-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
    
}
-(void)populateData
{
    //MARK: Hao Tran - kind of room (Queue room, rush room) is not apply in current
    /*
     NSInteger kindOfRoom = [(NSString *)[roomAssignment objectForKey:kRaKindOfRoom] integerValue];
     if (kindOfRoom == ENUM_KIND_OF_ROOM_RUSH) {
     [imgRushQueueRoom setHidden:NO];
     [imgRushQueueRoom setImage:[UIImage imageNamed:@"icon_rush room.png"]];
     }
     else if (kindOfRoom == ENUM_KIND_OF_ROOM_QUEUE) {
     [imgRushQueueRoom setHidden:NO];
     [imgRushQueueRoom setImage:[UIImage imageNamed:@"icon_queue room.png"]];
     }
     else if (kindOfRoom == ENUM_KIND_OF_ROOM_NORMAL) {
     [imgRushQueueRoom setHidden:YES];
     }
     else {
     
     }
     */
    
    RoomStatusModelV2 *roomStatus = [[RoomStatusModelV2 alloc] init];
    roomStatus.rstat_Id = (int)roomSelected.addjob_room_status_id;
    roomStatus = [[RoomManagerV2 sharedRoomManager] loadRoomStatusData:roomStatus];
    
    if(roomStatus.rstat_Image){
        NSData *imageData = roomStatus.rstat_Image;
        UIImage *imageRoomStatus = [UIImage imageWithData:imageData];
        [imgRoomStatus setImage:imageRoomStatus];
    }
    
    NSString *roomStatusCode = nil;
    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE]){
        roomStatusCode = roomStatus.rstat_Name;
    } else {
        roomStatusCode = roomStatus.rstat_Lang;
    }
    
    if(roomStatusCode.length <= 0){ //No have room status code
        roomStatusCode = [RoomManagerV2 getRoomStatusCodeByEnum:(int)roomSelected.addjob_room_status_id];
    }
    
    [lblRoomStatus setText:roomStatusCode];
    [lblRoomStatus setTextColor:[self colorForLabel:(int)roomSelected.addjob_room_status_id]];
    
    /*
    if (roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_VD
        || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_VC
        || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_VCB
        || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_VI
        || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_VPU) {
        [btnGuestInfo setUserInteractionEnabled:NO];
        [btnGuestInfo setBackgroundImage:[UIImage imageNamed:imgGuestInfoInactive] forState:UIControlStateNormal];
    }
    else if (roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_OC
             || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_OD
             || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_OI
             || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_OOO
             || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_OOS
             || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_OPU
             || roomSelected.addjob_room_status_id == ENUM_ROOM_STATUS_OC_SO) {
        [btnGuestInfo setUserInteractionEnabled:YES];
        [btnGuestInfo setBackgroundImage:[UIImage imageNamed:imgGuestInfoActive] forState:UIControlStateNormal];
    }
     */
    int result = [[RoomManagerV2 sharedRoomManager] updateGuestInfoByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:roomSelected.addjob_room_number];
    if(result == RESPONSE_STATUS_OK){
        isAlreadyGetGuest = YES;
        [self loadGuestData];
    }
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableGuestInfo] && listGuestModels.count > 0){
        [btnGuestInfo setUserInteractionEnabled:YES];
        [btnGuestInfo setBackgroundImage:[UIImage imageBeforeiOS7:imgGuestInfoActive equaliOS7:imgGuestInfoActiveFlat] forState:UIControlStateNormal];
    } else {
        [btnGuestInfo setUserInteractionEnabled:NO];
        [btnGuestInfo setBackgroundImage:[UIImage imageBeforeiOS7:imgGuestInfoInactive equaliOS7:imgGuestInfoInactiveFlat] forState:UIControlStateNormal];
    }
}

#pragma mark - Load Data

-(void)loadGuestViews
{
    if(listGuestModels.count > 0) {
        
        listGuestViews = [NSMutableArray array];
        
        //Scroll View
        CGRect guestScrollViewFr = scrGuests.frame;
        CGFloat maxHeightContent = guestScrollViewFr.size.height;
        
        for (int i = 0; i < listGuestModels.count; i ++) {
            GuestInfoModelV2 *curGuest = [listGuestModels objectAtIndex:i];
            
            GuestInfoViewV2 *guestInfoView = [[GuestInfoViewV2 alloc] initWithNibName:@"GuestInfoViewV2" bundle:nil];
            guestInfoView.parentVC = self;
            [guestInfoView setDataModel:curGuest];
            guestInfoView.roomNumber = roomSelected.addjob_room_number;
            //[guestInfoView.view setHidden:YES];
            [listGuestViews addObject:guestInfoView];
            [scrGuests addSubview:guestInfoView.view];
            
            //Set position for Guest View
            CGRect guestInfoViewFr = guestInfoView.view.frame;
            guestInfoViewFr.origin.x = guestScrollViewFr.size.width * i;
            guestInfoViewFr.origin.y = 0;
            [guestInfoView.view setFrame:guestInfoViewFr];
            
            [guestInfoView reloadView];
            
            //Find max height contain for scroll view
            if(guestInfoViewFr.size.height > maxHeightContent) {
                maxHeightContent = guestInfoViewFr.size.height;
            }
        }
        
        //Change content size for Scrollview
        [scrGuests setContentSize:CGSizeMake(scrGuests.frame.size.width * listGuestModels.count, maxHeightContent)];
        
        //Set up indicators highlight
        if(listGuestModels.count > 1) {
            guestIndicatorLeft = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 5, self.view.frame.size.height)];
            [guestIndicatorLeft setBackgroundColor:[UIColor clearColor]];
            [guestIndicatorLeft setHidden:YES];
            [guestIndicatorLeft setImage:[UIImage imageNamed:imgMenuLeft]];
            
            guestIndicatorRight = [[UIImageView alloc] initWithFrame:CGRectMake(scrContent.frame.size.width - 5, 0, 5, self.view.frame.size.height)];
            [guestIndicatorRight setImage:[UIImage imageNamed:imgMenuRight]];
            [guestIndicatorRight setBackgroundColor:[UIColor clearColor]];
            [guestIndicatorRight setHidden:YES];
            
            [self.view addSubview:guestIndicatorLeft];
            [self.view addSubview:guestIndicatorRight];
        }
    }
}

-(void) loadGuestData
{
    NSMutableArray *listCurrentGuests = [[RoomManagerV2 sharedRoomManager] loadGuestsByUserId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:roomSelected.addjob_room_number guestType:GuestTypeCurrent];
    NSMutableArray *listArrivalGuests = [[RoomManagerV2 sharedRoomManager] loadGuestsByUserId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:roomSelected.addjob_room_number guestType:GuestTypeArrival];
    
    listGuestModels = [NSMutableArray array];
    [listGuestModels addObjectsFromArray:listCurrentGuests];
    [listGuestModels addObjectsFromArray:listArrivalGuests];
}

-(void)loadData {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    int userId = [UserManagerV2 sharedUserManager].currentUser.userId;
    [listDetailDisplay removeAllObjects];
    
    //Load additional job details and categories
    NSMutableArray *listAddJobDetails = [addJobManager loadAddJobDetailsByUserId:userId roomId:(int)roomSelected.addjob_room_id roomAssignId:(int)roomSelected.addjob_room_assign_id];
    bool isEnglishLang = [[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@",ENGLISH_LANGUAGE];
    
    for (AddJobDetailModelV2 *curDetail in listAddJobDetails) {
        DetailsDisplay *displayCell = [[DetailsDisplay alloc] init];
        displayCell.addJobDetail = curDetail;
        AddJobCategoryModelV2 *category = [addJobManager loadAddJobCategoryByUserId:userId categoryId:(int)curDetail.addjob_detail_category_id];
        if(category != nil) {
            if(isEnglishLang){
                displayCell.addJobCategoryString = category.addjob_category_name;
            } else {
                displayCell.addJobCategoryString = category.addjob_category_name_lang;
            }
        }
        
        [listDetailDisplay addObject:displayCell];
    }
    
    //Reload Additional Job Room to show remark content
    AddJobRoomModelV2 *addJobRoom = [addJobManager loadAddJobRoomByUserId:(int)roomSelected.addjob_room_user_id roomId:(int)roomSelected.addjob_room_id roomAssignId:(int)roomSelected.addjob_room_assign_id searchId:(int)roomSelected.addjob_room_search_id];
    if(addJobRoom){
        roomSelected = addJobRoom;
    }
    
    //For showing room remark at the same as Room Detail
    int type = RemarkType_RoomRemark;
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isRemarkConsolidation]) {
        type = RemarkType_ConsolidationRemark;
    }
    NSString *remarkWS = [[RoomManagerV2 sharedRoomManager] getRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:type roomNumber:roomSelected.addjob_room_number];
    [[UserManagerV2 sharedUserManager] setCommontUserConfig:[NSString stringWithFormat:@"%@%@", REMARK_CONSOLIDATE, roomSelected.addjob_room_number] value:remarkWS.length > 0 ? remarkWS : @""];
    roomSelected.addjob_room_remark = remarkWS;
    
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
    
    [tbvContent reloadData];
    [tbvContent layoutIfNeeded];
    
    TopbarViewV2 *topbar = [self getTopBarView];
    BOOL isTopbarShowing = (topbar.isHidden != YES || topbar.alpha > 0.0f);
    [self modifyAllViews:isTopbarShowing];
}

#pragma mark - Delegate Remark View
-(void)remarkViewV2DoneWithText:(NSString *)text andController:(UIViewController *)controllerView{
    roomSelected.addjob_room_remark = text;
    [addJobManager updateAddJobRoom:roomSelected];
    [tbvContent reloadData];
}

//implement this for remove remark field with button cancel
-(void)remarkViewV2didDismissWithButtonIndex:(NSInteger)buttonIndez
{
    
}

-(void) remarkViewV2DoneWithText:(NSString *) text
{
    roomSelected.addjob_room_remark = text;
    [addJobManager updateAddJobRoom:roomSelected];
    
    //Update for Remark consolidation
    if([[UserManagerV2 sharedUserManager].currentUserAccessRight isRemarkConsolidation]) {
        [[RoomManagerV2 sharedRoomManager] updateRoomRemarksByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId remarkType:RemarkType_ConsolidationRemark roomNumber:roomSelected.addjob_room_number remarkValue:text];
    }
    
    [tbvContent reloadData];
}

#pragma mark - Pop up View Delegate

-(void)actionPopupViewDidDismissWithButtonIndex:(NSInteger)buttonIndex
{
    int buttonSetupStandard = 0;
    //Button Setup Standard
    if(buttonIndex == buttonSetupStandard)
    {
        [actionPopup removeFromSuperview];
        GuidelineViewV2* aGuidelineView = [[GuidelineViewV2 alloc] initWithNibName:@"GuidelineViewV2" bundle:nil];
        aGuidelineView.isPushFrom = IS_PUSHED_FROM_ADD_JOB_LIST;
        aGuidelineView.roomTypeId = (int)roomSelected.addjob_room_type_id;
        
        //set flag room is not completed
        [aGuidelineView setIsRoomCompleted:YES];
        
        [self.navigationController pushViewController:aGuidelineView animated:YES];
    }
}

#pragma mark  - Set Captions View
-(void)setCaptionsView {
    [self setTitle:[NSString stringWithFormat:@"%@ - %@ %@", [[LanguageManagerV2 sharedLanguageManager] getFind], [[LanguageManagerV2 sharedLanguageManager] getBy], [[LanguageManagerV2 sharedLanguageManager] getRoomTitle]]];
    
}

#pragma mark - === Handle Topbar Methods ===
#pragma mark

-(UIColor *) colorForLabel:(enum ENUM_ROOM_STATUS) roomStatus{
    switch (roomStatus) {
        case ENUM_ROOM_STATUS_VD:
        case ENUM_ROOM_STATUS_OD:
            return [UIColor redColor];
            
        case ENUM_ROOM_STATUS_VPU:
        case ENUM_ROOM_STATUS_OPU:
            return [UIColor yellowColor];
            
        case ENUM_ROOM_STATUS_OOO:
        case ENUM_ROOM_STATUS_OOS:
            return [UIColor grayColor];
            
        case ENUM_ROOM_STATUS_VC:
        case ENUM_ROOM_STATUS_OC:
        case ENUM_ROOM_STATUS_OC_SO:
            return [UIColor colorWithRed:0.0f green:1.0f blue:1.0f alpha:1.0f];
            
        case ENUM_ROOM_STATUS_VI:
        case ENUM_ROOM_STATUS_OI:
            return [UIColor greenColor];
            
        default:
            return [UIColor blackColor];
    }
}

-(void)modifyAllViews:(BOOL)isTopBarShowing
{
    TopbarViewV2 *topbar = [self getTopBarView];
    int heightTopbar = 0;
    if(isTopBarShowing) {
        heightTopbar = topbar.frame.size.height;
    }
    
    //ViewAction
    CGRect fViewAction = viewAction.frame;
    fViewAction.origin.y = heightTopbar;
    [viewAction setFrame:fViewAction];
    
    //more height to fit content table
    CGRect frTbvContent = tbvContent.frame;
    frTbvContent.size.height = tbvContent.contentSize.height; //[self totalHeightOfTableContent];
    [tbvContent setFrame:frTbvContent];
    
    //button Action
    CGRect frBtnAction = btnAction.frame;
    frBtnAction.origin.y = frTbvContent.size.height + 7;
    [btnAction setFrame:frBtnAction];
    
    //ScrollView Content
    CGRect fScrContent = scrContent.frame;
    if([DeviceManager getScreenDeviceHeight] == DeviceScreenSizeStandardHeight4_0) {
        fScrContent.size.height = 416 - heightTopbar;
    } else {
        fScrContent.size.height = 328 - heightTopbar;
    }
    fScrContent.origin.y = fViewAction.origin.y + fViewAction.size.height;
    [scrContent setContentSize:CGSizeMake(fScrContent.size.width, frBtnAction.origin.y + frBtnAction.size.height + 5)];
    [scrContent setFrame:fScrContent];
    [scrContent setScrollEnabled:YES];
    
    
}

-(void)loadTopbarView {
    TopbarViewV2 *topbar = [[TopbarViewV2 alloc] initViewWithSuperModel:[[CommonVariable sharedCommonVariable] superRoomModel]];
    [topbar setTag:tagTopbarView];
    [self.view addSubview:topbar];
    [topbar setHidden:YES];
    [topbar setAlpha:0.0];
    [self addButtonHandleShowHideTopbar];
}

-(void)showTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
    
    
}

-(void)hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void)addButtonHandleShowHideTopbar {
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [titleBarButtonFirst setTitle:roomSelected.addjob_room_number forState:UIControlStateNormal];
    
    [vBarButton addSubview:titleBarButtonFirst];
    titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 22, 200, 22)];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond setTitle:[[LanguageManagerV2 sharedLanguageManager] getAdditionalJobTitle] forState:UIControlStateNormal];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [vBarButton addSubview:titleBarButtonSecond];
    
    self.navigationItem.titleView = vBarButton;
    
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [titleBarButtonSecond setEnabled:NO];
    [titleBarButtonFirst setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.6f];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
    [titleBarButtonFirst setEnabled:YES];
    [titleBarButtonSecond setEnabled:YES];
}

-(void)adjustShowForViews {
    TopbarViewV2 *topBarView = [self getTopBarView];
    CGRect fTopbarView = topBarView.frame;
    
    CGRect fViewAction = viewAction.frame;
    fViewAction.origin.y += fTopbarView.size.height;
    [viewAction setFrame:fViewAction];
    
    CGRect fButtonAction = btnAction.frame;
    fButtonAction.origin.y += fTopbarView.size.height;
    [btnAction setFrame:fButtonAction];
    
    CGRect fTableContent = tbvContent.frame;
    fTableContent.size.height = fButtonAction.origin.y + fButtonAction.size.height; //self.tbvContent.contentSize.height + fTopbarView.size.height;
    [tbvContent setFrame:fTableContent];
    
    CGRect fScrViewContent = scrContent.frame;
    fScrViewContent.origin.y += fTopbarView.size.height;
    fScrViewContent.size.height -= fTopbarView.size.height;
    [scrContent setFrame:fScrViewContent];
    
//    CGRect fGuestInfoView = guestInfo.view.frame;
//    fGuestInfoView.size.height -= fTopbarView.size.height;
//    [guestInfo.view setFrame:fGuestInfoView];
    
    CGRect fScrGuest = scrGuests.frame;
    fScrGuest.origin.y += fTopbarView.size.height;
    fScrGuest.size.height -= fTopbarView.size.height;
    [scrGuests setFrame:fScrGuest];
    
}

-(void)adjustRemoveForViews {
    TopbarViewV2 *topBarView = [self getTopBarView];
    CGRect fTopbarView = topBarView.frame;
    
    CGRect fViewAction = viewAction.frame;
    fViewAction.origin.y -= fTopbarView.size.height;
    [viewAction setFrame:fViewAction];
    
    CGRect fButtonAction = btnAction.frame;
    fButtonAction.origin.y -= fTopbarView.size.height;
    [btnAction setFrame:fButtonAction];
    
    CGRect fTableContent = tbvContent.frame;
    fTableContent.size.height = tbvContent.contentSize.height;
    [tbvContent setFrame:fTableContent];
    
    CGRect fScrViewContent = scrContent.frame;
    fScrViewContent.origin.y -= fTopbarView.size.height;
    fScrViewContent.size.height += fTopbarView.size.height;
    [scrContent setFrame:fScrViewContent];
    
//    CGRect fGuestInfoView = guestInfo.view.frame;
//    fGuestInfoView.size.height += fTopbarView.size.height;
//    [guestInfo.view setFrame:fGuestInfoView];
    
    CGRect fScrGuest = scrGuests.frame;
    fScrGuest.origin.y -= fTopbarView.size.height;
    fScrGuest.size.height += fTopbarView.size.height;
    [scrGuests setFrame:fScrGuest];
}

-(IBAction)btnRoomAssignmentInfo:(id)sender
{
    if (listGuestModels.count > 0) {
        [tbvContent setHidden:NO];
        [scrContent setHidden:NO];
        [scrGuests setHidden:YES];
        [guestIndicatorLeft setHidden:YES];
        [guestIndicatorRight setHidden:YES];
        [btnAction setHidden:NO];
        
        TopbarViewV2 *topbar = [self getTopBarView];
        BOOL isTopbarShowing = (topbar.isHidden != YES || topbar.alpha > 0.0f);
        
        [self modifyAllViews:isTopbarShowing];
    }
}

-(IBAction)btnGuestInfoClicked:(id)sender
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    if(isAlreadyGetGuest && [[UserManagerV2 sharedUserManager].currentUserAccessRight isEnableGuestInfo]){
        //Not used anymore
        //bool isSuccess = [addJobManager loadWSAddJobGuestInfoByUserId:[UserManagerV2 sharedUserManager].currentUser.userId roomId:roomSelected.addjob_room_id];
        
//        int result = [[RoomManagerV2 sharedRoomManager] updateGuestInfoByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId roomNumber:roomSelected.addjob_room_number];
//        if(result == RESPONSE_STATUS_OK){
//            isAlreadyGetGuest = YES;
//            //[self loadGuestData];
            [self loadGuestViews];
//        }
    }
    
    if(listGuestModels.count <= 0) { //Don't have Guest Info
        [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
        [[HomeViewV2 shareHomeView] endWaitingLoadingData];
        UIAlertView *alert = [[CustomAlertViewV2 alloc] initWithArrayButtons:[NSArray arrayWithObjects:[UIImage imageNamed:@"yes_bt.png"], [UIImage imageNamed:@"no_bt.png"], nil] title:@"" message:[L_guest_do_not_exist currentKeyToLanguage] delegate:self cancelButtonTitle:[[LanguageManagerV2 sharedLanguageManager] getOK] otherButtonTitles: nil];
        [alert show];
        return; //break
    }
    
    [tbvContent setHidden:YES];
    [btnAction setHidden:YES];
    [scrContent setHidden:YES];
    [scrGuests setHidden:NO];
    //Check and show indicators
    [self scrollViewDidScroll:scrGuests];
    
    [self performSelector:@selector(hiddenHUDAfterSaved:) withObject:HUD afterDelay:0.0f];
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

#pragma mark - === Handle Back Button ===
#pragma mark

-(IBAction)btnActionClicked:(id)sender
{
    if(actionPopup == nil) {
        NSMutableArray *buttons = [NSMutableArray array];
        [buttons addObject:[L_GUIDE_LINE currentKeyToLanguage]];
        actionPopup = [[ActionPopupView alloc] initWithTitles:[NSArray arrayWithArray:buttons] delegate:(id<ActionPopupDelegate>)self option:YES];
    }
    
    //[self.tabBarController.view addSubview:actionPopup];
    [self.tabBarController addSubview:actionPopup];
}


-(void)backBarPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addBackButton {
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backBarPressed) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == scrGuests) {
        if(listGuestModels.count > 1) {
            // Switch the indicator when more than 50% of the previous/next page is visible
            CGFloat pageWidth = scrollView.frame.size.width;
            int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            if(page == 0) { //First Page
                [guestIndicatorLeft setHidden:YES];
                [guestIndicatorRight setHidden:NO];
            } else if(page == listGuestModels.count - 1) { //Last Page
                [guestIndicatorLeft setHidden:NO];
                [guestIndicatorRight setHidden:YES];
            } else {
                [guestIndicatorLeft setHidden:NO];
                [guestIndicatorRight setHidden:NO];
            }
        }
    }
}

#pragma mark - iOS 7 Resources
-(void)loadFlatResource
{
    [btnAction setBackgroundImage:[UIImage imageNamed:imgCleaningStatusBtnAndActionBtnFlat] forState:UIControlStateNormal];
    [btnAction setTitle:[L_action currentKeyToLanguage] forState:UIControlStateNormal];
    [btnRoomAssignment setBackgroundImage:[UIImage imageNamed:imgGuestInfoActiveFlat] forState:UIControlStateNormal];
    [lblGuestInfo setText:[L_GUEST_INFO currentKeyToLanguage]];
}
@end
