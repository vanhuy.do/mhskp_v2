//
//  AdditionalJobRoomCellV2.h
//  mHouseKeeping


#import <UIKit/UIKit.h>

@interface AdditionalJobRoomCellV2 : UITableViewCell {
    IBOutlet UILabel *lblRoomNo;
    IBOutlet UILabel *lblAddJobNames;
    IBOutlet UILabel *lblRoomStatus;
}

@property (nonatomic, strong) IBOutlet UILabel *lblRoomNo;
@property (nonatomic, strong) IBOutlet UILabel *lblAddJobNames;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomStatus;
@property (nonatomic, strong) IBOutlet UILabel *lblRoomType;
@property (nonatomic, strong) IBOutlet UIImageView *imgAddJobStatus;

//@property (nonatomic, strong) IBOutlet UIButton *btnDetailArrow;

@end
