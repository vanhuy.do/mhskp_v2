//
//  AdditionalJobRoomCellV2.m
//  mHouseKeeping
//
//  Created by Nghia Truong on 2/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AdditionalJobRoomCellV2.h"

@implementation AdditionalJobRoomCellV2

@synthesize lblRoomNo, lblRoomStatus, lblAddJobNames;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:
    (NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)dealloc {
//    [lblVIP release];
//    [lblRoomNo release];
//    [lblRoomStatus release];
//    [lblGuestName release];
//    [btnDetailArrow release];
//    [imgCleaningStatus release];
//    [imgRoomStatus release];
//    [super dealloc];
}

@end
