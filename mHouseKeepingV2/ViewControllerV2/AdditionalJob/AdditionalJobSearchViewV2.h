//
//  AdditionalJobSearchViewV2.h
//  eHouseKeeping
//

#import <UIKit/UIKit.h>
#import "AdditionalJobManagerV2.h"

@protocol AdditionalJobSearchViewDelegate <NSObject>

@optional
//-(void)didSelectAdditionalJob:(AddJobModelV2*)addJobData;

//delegate of list AddJobModelV2
-(void)didSelectAdditionalJobs:(NSMutableArray*)listSearchsAddJobs;
@end

@interface AdditionalJobSearchViewV2 : UIViewController <UITableViewDataSource,UITableViewDelegate, UIAlertViewDelegate>{
    
    NSMutableArray *addJobItemsList;
    TopbarViewV2 *topBarView;
    AdditionalJobManagerV2 *addJobManager;
}

@property (nonatomic, assign) id<AdditionalJobSearchViewDelegate> delegate;
@property (nonatomic, strong) TopbarViewV2 *topBarView;
@property (nonatomic, strong) IBOutlet UITableView *tableAddJobs;
@property (nonatomic, strong) IBOutlet UIButton *buttonSearch;
@property (nonatomic, strong) NSMutableArray *listAddJobsSelected;

- (void)loadData;

@end
