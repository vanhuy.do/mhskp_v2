//
//  AdditionalJobSearchView.m
//  eHouseKeeping

#import "AdditionalJobSearchViewV2.h"
#import "DateTimeUtility.h"
#import "ehkDefines.h"
#import "LanguageManager.h"
#import "TasksManagerV2.h"
//#import "HomeViewV2.h"
#import "MyNavigationBarV2.h"
#import "LanguageManagerV2.h"
#import "DeviceManager.h"

#define highlightImage      @"tab_count_active.png"
#define normalImage         @"tab_count_NOactive.png"
#define background          @"bg.png"

#define kFontLabel @"Arial-BoldMT"

@interface AdditionalJobSearchViewV2 (PrivateMethods)
-(TopbarViewV2 *)getTopBarView;
-(void) showTopbarView;
-(void) hideTopbarView;
-(void) adjustShowForViews;
-(void) adjustRemoveForViews;
-(void)setHiddenTopbarView;
-(void) topbarTapped:(UIButton *) sender;
@end

@implementation AdditionalJobSearchViewV2
@synthesize tableAddJobs,
topBarView,
delegate,
buttonSearch,
listAddJobsSelected;

int tagCellLabel = 88;
int tagCellImageSelect = 89;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - View lifecycle

-(IBAction)buttonSearchPressed:(id)sender
{
    if(listAddJobsSelected != nil)
    {
        [delegate didSelectAdditionalJobs:listAddJobsSelected];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[L_please_choose_search currentKeyToLanguage] delegate:nil cancelButtonTitle:[L_OK currentKeyToLanguage] otherButtonTitles: nil];
        [alert show];
    }
}

- (void)loadData {
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.tabBarController.view];
    [HUD setLabelText:[[LanguageManagerV2 sharedLanguageManager] getLoadingData]];
    [self.tabBarController.view addSubview:HUD];
    [HUD show:YES];
    
    //enable flag to announce, is loading data
    [[HomeViewV2 shareHomeView] waitingLoadingData];
    
    [addJobItemsList removeAllObjects];
    [addJobManager loadWSAddJobItemListByUserId:[UserManagerV2 sharedUserManager].currentUser.userId hotelId:[UserManagerV2 sharedUserManager].currentUser.userHotelsId];
    addJobItemsList = [addJobManager loadAllAddJobItemByUserId:[UserManagerV2 sharedUserManager].currentUser.userId];
    
    [tableAddJobs setBackgroundColor:[UIColor clearColor]];
    [tableAddJobs setDataSource:self];
    [tableAddJobs setDelegate:self];
    [tableAddJobs reloadData];    
    
    [self hiddenHUDAfterSaved:HUD];
    //end loading data
    [[HomeViewV2 shareHomeView] endWaitingLoadingData];
}

-(void) hiddenHUDAfterSaved:(MBProgressHUD *)HUD{
    [HUD hide:YES];
    [HUD removeFromSuperview];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        //Clear redundant view for header, footter
        CGRect frameHeaderFooter = tableAddJobs.tableHeaderView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *headerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tableAddJobs setTableHeaderView:headerView];
        
        frameHeaderFooter = tableAddJobs.tableFooterView.frame;
        frameHeaderFooter.size.height = 0.1f;
        UIView *footerView = [[UIView alloc] initWithFrame:frameHeaderFooter];
        [tableAddJobs setTableFooterView:footerView];
        [self loadFlatResource];
    }
    
    //init arrayAdditionalJobSearch
    addJobItemsList = [NSMutableArray array];
    addJobManager = [AdditionalJobManagerV2 sharedAddionalJobManager];
    
    UIColor *bgColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:background]];
    self.view.backgroundColor = bgColor;
    
    if (topBarView == nil) {
        topBarView = [[TopbarViewV2 alloc] initViewWithSuperModel:[[SuperRoomModelV2 alloc] init]];
        [topBarView setTag:tagTopbarView];
        [self.view addSubview:topBarView];
        [topBarView setHidden:YES];
    }
    
    [self addButtonHandleShowHideTopbar];
    
    // Load Data
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.0f];
    
}

-(void)loadView {
    [super loadView];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}



#pragma mark - Alert view Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
}

#pragma mark - tableView Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [addJobItemsList count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UITableViewCell *additionalJobSearchCell = nil;
    static NSString *additionalJobSearchIdentifier = @"AdditionalJobSearchCell";
    
    additionalJobSearchCell = [tableView dequeueReusableCellWithIdentifier:additionalJobSearchIdentifier];
    
    if (additionalJobSearchCell == nil) {
        
        additionalJobSearchCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:additionalJobSearchIdentifier];
        [additionalJobSearchCell.textLabel setTextAlignment:NSTextAlignmentCenter];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 5, 260, 33)];
        label.tag = tagCellLabel;
        [label setFont:[UIFont fontWithName:kFontLabel size:20]];
        [label setTextColor:[UIColor colorWithRed:6.0f/255.0f green:62.0f/255.0f blue:127.0f/255.0f alpha:1]];
        [label setTextAlignment:NSTextAlignmentLeft];
        [additionalJobSearchCell.contentView addSubview:label];
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(6, 5, 33, 33)];
        [imgView setImage:[UIImage imageNamed:@"check.png"]];
        imgView.tag = tagCellImageSelect;
        [additionalJobSearchCell.contentView addSubview:imgView];
        [imgView setHidden:YES];
    }
        
    /********************** Text ****************************************/           
    UILabel *label = (UILabel*)[additionalJobSearchCell.contentView viewWithTag:tagCellLabel];
    [label setBackgroundColor:[UIColor clearColor]];
    
    AddJobItemModelV2 *curAddJob = [addJobItemsList objectAtIndex:indexPath.row];
    if([[LanguageManagerV2 sharedLanguageManager] currentLanguageIsEqualTo:@"%@", ENGLISH_LANGUAGE]) {
        label.text = curAddJob.addjob_item_name;
    } else {
        label.text = curAddJob.addjob_item_name_lang;
    }
        
    UIImageView *imgView = (UIImageView*)[additionalJobSearchCell.contentView viewWithTag:tagCellImageSelect];
    if([self indexSelectedAddJob:curAddJob] >= 0)
    {
        [imgView setHidden:NO];
    }
    else
    {
        [imgView setHidden:YES];
    }
    
    /********************** Text ****************************************/
    
    
    return additionalJobSearchCell;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//return >= 0 if found in selected list
//return <0 if not found in selected list
-(int)indexSelectedAddJob:(AddJobItemModelV2*)addJob
{
    if ([listAddJobsSelected count] > 0) {
        int indexSelected = 0;
        for (AddJobItemModelV2 *curAddJob in listAddJobsSelected) {
            if(curAddJob.addjob_item_id == addJob.addjob_item_id) {
                return indexSelected;
            }
            indexSelected ++;
        }
    }
    
    return -1;
}

//Set select or deselect data
-(void)setSelectedAddJobs:(AddJobItemModelV2*)addJobSelected
{
    if(listAddJobsSelected == nil) {
        listAddJobsSelected = [[NSMutableArray alloc] init];
    }
    
    int indexSelected = [self indexSelectedAddJob:addJobSelected];
    if(indexSelected >= 0)
    {
        [listAddJobsSelected removeObjectAtIndex:indexSelected];
    }
    else
    {
        [listAddJobsSelected addObject:addJobSelected];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AddJobItemModelV2 *addJobSelected = [addJobItemsList objectAtIndex:indexPath.row];
    [self setSelectedAddJobs:addJobSelected];
    
    [tableView reloadData];
//    [tableView cellForRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - TopbarViewV2
-(TopbarViewV2 *)getTopBarView {
    return (TopbarViewV2 *)[self.view viewWithTag:tagTopbarView];
}

-(void) showTopbarView{
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(setHiddenTopbarView)];
    
    f = topbar.frame;
    f.origin.y = 0;
    [topbar setFrame:f];
    [topbar setAlpha:1.0];
    
    [self adjustShowForViews];
    
    [UIView commitAnimations];
}

-(void) hideTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDidStopSelector:@selector(setHiddenTopbarView)];
    [UIView setAnimationDelegate:self];
    
    f.origin.y = - f.size.height;
    [topbar setFrame:f];
    [topbar setAlpha:0.0];
    
    [self adjustRemoveForViews];
    
    [UIView commitAnimations];
}

-(void) adjustShowForViews {
    //adjust all views when show 
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = tableAddJobs.frame;
    [tableAddJobs setFrame:CGRectMake(0, f.size.height, 320, ftbv.size.height - f.size.height)];
}
                           
-(void) adjustRemoveForViews {
    //adjust all views when hide
    TopbarViewV2 *topbar = [self getTopBarView];
    CGRect f = topbar.frame;
    
    CGRect ftbv = tableAddJobs.frame;
    [tableAddJobs setFrame:CGRectMake(0, 0, 320, ftbv.size.height + f.size.height)];                          
}

-(void)topbarTapped:(UIButton *)sender {
    TopbarViewV2 *topbar = [self getTopBarView];
    [sender setEnabled:NO];
    if (topbar.isHidden) {
        [self showTopbarView];
    } else {
        [self hideTopbarView];
    }
    
    [self performSelector:@selector(topbarButtonEnable:) withObject:sender afterDelay:0.5];
}

-(void) topbarButtonEnable:(UIButton *) topbarBtn {
    [topbarBtn setEnabled:YES];
}

-(void)setHiddenTopbarView {
    TopbarViewV2 *topbar = [self getTopBarView];
    [topbar setHidden:!topbar.hidden];
}

-(void)addButtonHandleShowHideTopbar {
    SuperRoomModelV2 *modelSuper = [[SuperRoomModelV2 alloc] init];
    NSString* propertyName = [[RoomManagerV2 sharedRoomManager] getPropertyName:modelSuper];
    NSString* propertyNameStr = [NSString stringWithFormat:@"%@",propertyName== nil ? @"":propertyName];
    
    UIView *vBarButton = [[UIView alloc] initWithFrame:FRAME_BUTTON_TOPBAR];
    UIButton *titleBarButtonFirst = [[UIButton alloc] initWithFrame:CGRectMake(0, -5, 200, 30)];
    [titleBarButtonFirst setTag:1];
    titleBarButtonFirst.titleLabel.backgroundColor = [UIColor clearColor];
    [titleBarButtonFirst.titleLabel setFont: FONT_BUTTON_TOPBAR];
    titleBarButtonFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleBarButtonFirst.titleLabel setAdjustsFontSizeToFitWidth:YES];
    titleBarButtonFirst.titleLabel.textColor = COLOR_BUTTON_TOPBAR;
    
    [titleBarButtonFirst.titleLabel setShadowColor:SHADOW_COLOR_BUTTON_TOPBAR];
    [titleBarButtonFirst.titleLabel setShadowOffset:SHADOW_OFFSET_BUTTON_TOPBAR];
    [titleBarButtonFirst addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonFirst setTitle:propertyNameStr forState:UIControlStateNormal];
    [vBarButton addSubview:titleBarButtonFirst];
    
    UIButton *titleBarButtonSecond = [[UIButton alloc] initWithFrame:CGRectMake(0, 23, 200, 22)];
    [titleBarButtonSecond setTag:2];
    [titleBarButtonSecond setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBarButtonSecond.titleLabel setFont:FONT_BUTTON_TOPBAR_SECOND];
    [titleBarButtonSecond addTarget:self action:@selector(topbarTapped:) forControlEvents:UIControlEventTouchUpInside];
    [titleBarButtonSecond setTitle:[[RoomManagerV2 sharedRoomManager] getHouseKeeperName] forState:UIControlStateNormal];
    [vBarButton addSubview:titleBarButtonSecond];
    
    [self.navigationItem setTitleView:vBarButton];
    //Move title to center vertical
    CGRect titleViewBounds = self.navigationItem.titleView.bounds;
    int moveLeftTitle = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        moveLeftTitle = 10;
    }
    self.navigationItem.titleView.bounds = CGRectMake(titleViewBounds.origin.x + moveLeftTitle, Y_TOPBAR_TITLE, titleViewBounds.size.width, titleViewBounds.size.height);
    
    [self addBackButton];

}
-(void)addBackButton {
    
    //back button navigation
    self.navigationItem.leftBarButtonItem = [MyNavigationBarV2 createBackButtonWithTarget:self selector:@selector(backView) forControlEvents:UIControlEventTouchUpInside];
}

-(void)backView
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) hiddenHeaderView {      
    BOOL isHidden = topBarView.hidden;
    [topBarView setHidden:!isHidden];
    
    CGRect vAdditionalJobSearchFrame = tableAddJobs.frame;
    
    CGFloat scale;
    if (isHidden) {
        scale = 45;
    }
    else {
        scale = -45;
    }
    
    vAdditionalJobSearchFrame.origin.y += scale;
    vAdditionalJobSearchFrame.size.height -= scale;
    
    [tableAddJobs setFrame:vAdditionalJobSearchFrame];
}

-(void)loadFlatResource
{
    [buttonSearch setBackgroundImage:[UIImage imageNamed:imgBtnSubmitFlat] forState:UIControlStateNormal];
    [buttonSearch setBackgroundImage:[UIImage imageNamed:imgBtnSubmitClickedFlat] forState:UIControlStateHighlighted];
    [buttonSearch setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_SEARCH] forState:UIControlStateNormal];
}

@end
