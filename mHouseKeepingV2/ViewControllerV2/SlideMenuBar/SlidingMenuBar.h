//
//  SlidingMenuBar.h
//  mHouseKeeping
//
//  Created by vinhnguyen on 11/19/13.
//
//

#import <UIKit/UIKit.h>

//Content side belong to SlidingMenuBarDock
#define SlidingWidthButton 80
#define SlidingWidthScrollViewLegend 260
#define SlidingHeightButton 56

#define TagMovingButton 57
#define SlidingBackgroundColor [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.3f]
#define MovingBackgroundButton [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:0.5f]

enum SlidingMenuBarDock {
    SlidingMenuBarDockLeft,
    SlidingMenuBarDockRight,
    SlidingMenuBarDockTop,
    SlidingMenuBarDockBottom
};

enum SlidingType {
    SlidingType_Menu = 0,
    SlidingType_Legend = 1,
};

@protocol SlidingMenuBarDelegate <NSObject>

@optional

-(void)didSelectedButtonIndex:(int)index tag:(int)tagOfButton;

@end

@interface SlidingMenuBar : UIViewController<UINavigationControllerDelegate, UIScrollViewDelegate>
{
    UIScrollView *scrollViewContent;
    NSMutableArray *buttonList;
    CGRect superViewFrame;
    enum SlidingMenuBarDock dockType;
    UIButton *movingButton;
    UIImageView *selectedImage;
    CGFloat xMenuBarShow, yMenuBarShow;
}

//@property (atomic, assign) enum SlidingMenuBarDock dockType;
@property (atomic, assign) BOOL isShowing;
@property (atomic, assign) int currentSelected;
@property (atomic, assign) enum SlidingType slidingType;
@property (nonatomic, assign) id<SlidingMenuBarDelegate> delegate;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollViewLegend;

//Legend properties
@property (nonatomic, strong) IBOutlet UIImageView *imgLegendPending;
@property (nonatomic, strong) IBOutlet UIImageView *imgLegendReassignPending;
@property (nonatomic, strong) IBOutlet UIImageView *imgLegendReassignComplete;
@property (nonatomic, strong) IBOutlet UIImageView *imgLegendMakeUpReassign;
@property (nonatomic, strong) IBOutlet UIImageView *imgLegendDND;
@property (nonatomic, strong) IBOutlet UIImageView *imgLegendServiceLater;
@property (nonatomic, strong) IBOutlet UIImageView *imgLegendDeclinedService;
@property (weak, nonatomic) IBOutlet UIImageView *imgLegendDoubleLock;

@property (nonatomic, strong) IBOutlet UIImageView *imgLegendStart;
@property (nonatomic, strong) IBOutlet UIImageView *imgLegendPause;
@property (nonatomic, strong) IBOutlet UIImageView *imgLegendStop;
@property (nonatomic, strong) IBOutlet UIImageView *imgLegendComplete;
@property (nonatomic, strong) IBOutlet UIImageView *imgLegendRush;
@property (nonatomic, strong) IBOutlet UIImageView *imgLegendQueue;
@property (nonatomic, strong) IBOutlet UIImageView *imgLegendReleaseOOS;
@property (nonatomic, strong) IBOutlet UIImageView *imgLegendProfileNote;
@property (nonatomic, strong) IBOutlet UIImageView *imgLegendStayOver;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendPending;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendReassignPending;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendReassignComplete;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendMakeUpReassign;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendDND;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendServiceLater;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendDeclinedService;
@property (weak, nonatomic) IBOutlet UILabel *lblLegendDoubleLock;

@property (strong, nonatomic) IBOutlet UILabel *lblLegendStart;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendPause;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendStop;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendComplete;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendRush;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendQueue;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendReleaseOOS;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendProfileNote;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendStayOver;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendPreviousDay;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendDueIn;
@property (strong, nonatomic) IBOutlet UILabel *lblLegendDueOut;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleLegend;

//-(id)initWithView:(UIView*)view dockType:(enum SlidingMenuBarDock) dockTypeValue swipeDetector:(BOOL)isSwipeToShow;
-(id)initWithView:(UIView*)superView dockType:(enum SlidingMenuBarDock)dockTypeValue slidingType:(int) slidingType;

-(void)setSyncingStatusButton:(BOOL)isSyncing;
-(void)removeButtonAtIndex:(int)indexRemove;

//Remove all menu buttons
-(void)removeAllButtons;

-(void)hideAll;
-(void)showAll;

-(void)hide:(BOOL) isAnimated;

//This method to paint all buttons at once time
//All buttons will paint in scrollViewContent
-(void)paintAllButtons;

//Create button and add to sliding bar menu
-(UIButton*)createButtonWithTag:(int)tagButtonMenu atIndex:(int)indexButton enabled:(BOOL) isButtonEnabled;

//Get the button with assigned tag
-(UIButton*)buttonWithTag:(int)tagButtonMenu;

//Highlight button menu with tag selected
-(void)highlightSelectedTag;

//reload all title buttons
-(void)reloadAllTitleButtons;

//reload all legend descriptions
-(void)reloadAllLegendDescriptions;

-(void)setAlwaysHoldPanicButton:(BOOL)isHolding;

@end
