//
//  SlidingMenuBar.m
//  mHouseKeeping
//
//  Created by vinhnguyen on 11/19/13.
//
//

#import "SlidingMenuBar.h"
#import "MyNavigationBarV2.h"
#import "ehkDefinesV2.h"
#import "LanguageManagerV2.h"
#import "DeviceManager.h"
#import "UIImage+Tint.h"

#define tagLeftHighlight 1001
#define tagRightHighlight 1002
#define tagTopHighlight 1003
#define tagBottomHighlight 1004

@implementation SlidingMenuBar

//@synthesize dockType;
@synthesize isShowing;
@synthesize delegate;
@synthesize currentSelected;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithView:(UIView*)superView dockType:(enum SlidingMenuBarDock)dockTypeValue slidingType:(int) slidingType
{
    //self = [super init];
    self = [[SlidingMenuBar alloc] initWithNibName:@"SlidingMenuBar" bundle:nil];
    if (self) {
        buttonList = [NSMutableArray array];
        currentSelected = (int)NSNotFound;
        xMenuBarShow = 0;
        yMenuBarShow = 0;
        
        //Add this Sliding Menu Bar to superview
        //UIWindow *window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
        //[window addSubview:self.view];
        //superViewFrame = window.frame;
        
        _slidingType = slidingType;
        
        [superView addSubview:self.view];
        superViewFrame = superView.frame;
        //[self.view setFrame:superViewFrame];
        
        if (_slidingType == SlidingType_Legend) {
            [self setupLegend];
            [self.view setBackgroundColor:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.8f]];
            scrollViewContent = _scrollViewLegend;
            [scrollViewContent setShowsVerticalScrollIndicator:NO];
            scrollViewContent.delegate = self;
            [self dockViewWithType:dockTypeValue];
        } else {
            [self.view setBackgroundColor:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.4f]];
            //Sliding content
            scrollViewContent = [[UIScrollView alloc] init];
            [scrollViewContent setShowsVerticalScrollIndicator:NO];
            scrollViewContent.delegate = self;
            [self dockViewWithType:dockTypeValue];
        }
        
        isShowing = YES;
        //DockView is Showing by default so we set hide for this.
        [self hide:NO];
        [self insertMovingButton];
        [self insertHighlightImages];
        
        //Create highlight image
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            selectedImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgActiveFlat]];
        } else {
            selectedImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgActive]];
        }
        
//        CGRect frame = selectedImage.frame;
//        frame.size.width = SlidingWidthButton;
//        frame.size.height = SlidingHeightButton - 2;
//        frame.origin.x = 0;
//        frame.origin.y = 0;
//        if(isSwipeToShow) {
//            if(dockType == SlidingMenuBarDockRight || dockType == SlidingMenuBarDockLeft) {
//                UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeftDetected:)];
//                [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
//                [superView addGestureRecognizer:swipeLeft];
//
//                UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRightDetected:)];
//                [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
//                [superView addGestureRecognizer:swipeRight];
//            } else {
//                UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUpDetected:)];
//                [swipeUp setDirection:UISwipeGestureRecognizerDirectionUp];
//                [superView addGestureRecognizer:swipeUp];
//                
//                UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDownDetected:)];
//                [swipeDown setDirection:UISwipeGestureRecognizerDirectionDown];
//                [superView addGestureRecognizer:swipeDown];
//            }
//        }
    }
    return self;
}

-(UIButton*)createButtonWithTag:(int)tagButtonMenu atIndex:(int)indexButton enabled:(BOOL) isButtonEnabled
{
    UIButton *butTabBar = [UIButton buttonWithType:UIButtonTypeCustom];
    [butTabBar setTag:tagButtonMenu];
    [butTabBar setEnabled:isButtonEnabled];
    [butTabBar setFrame:CGRectMake(0, 0, SlidingWidthButton, SlidingHeightButton)];
    [butTabBar addTarget:self action:@selector(didChoseButton:) forControlEvents:UIControlEventTouchDown];
    [butTabBar setTitleEdgeInsets:UIEdgeInsetsMake(SlidingHeightButton/2 + 9, 0, 0, 0)];
    UIFont *buttabfont = [UIFont boldSystemFontOfSize:12];
    //[butTabBar setTitleColor:[UIColor colorWithRed:4.0f/255.0 green:62.0f/255.0 blue:48.0f/255.0 alpha:1.0] forState:UIControlStateNormal];
    
    //Set font color
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [butTabBar setTitleColor:[UIColor colorWithRed:255.0f green:255.0f blue:255.0f alpha:0.9f] forState:UIControlStateNormal];
    } else {
        [butTabBar setTitleColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1.0] forState:UIControlStateNormal];
    }
    [butTabBar.titleLabel setFont:buttabfont];

    //Set title menu button with current language
    [self reloadTitleButton:butTabBar];
    
    if((int)indexButton == (int)NSNotFound) {
        [buttonList addObject:butTabBar];
    } else {
        [buttonList insertObject:butTabBar atIndex:indexButton];
    }
    
    return butTabBar;
    
}

//reload all title buttons
-(void)reloadAllTitleButtons
{
    for (UIButton *curButton in buttonList) {
        [self reloadTitleButton:curButton];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

//This method to paint all buttons at once time
//All buttons will paint in scrollViewContent
-(void)paintAllButtons
{
    if(dockType == SlidingMenuBarDockLeft) {
    
        int indexButton = 0;
        CGSize contentSize = CGSizeMake(SlidingWidthButton, buttonList.count * SlidingHeightButton);
        [scrollViewContent setContentSize:contentSize];
        for (UIButton *curButton in buttonList) {
            CGRect buttonFrame = curButton.frame;
            buttonFrame.origin.y = indexButton * SlidingWidthButton;
            [curButton setFrame:buttonFrame];
            [scrollViewContent addSubview:curButton];
            indexButton ++;
        }
    } else if(dockType == SlidingMenuBarDockTop) {
        
        int indexButton = 0;
        CGSize contentSize = CGSizeMake(buttonList.count * SlidingWidthButton, SlidingHeightButton);
        [scrollViewContent setContentSize:contentSize];
        for (UIButton *curButton in buttonList) {
            CGRect buttonFrame = curButton.frame;
            buttonFrame.origin.x = indexButton * SlidingWidthButton;
            [curButton setFrame:buttonFrame];
            [scrollViewContent addSubview:curButton];
            indexButton ++;
        }
    } else if(dockType == SlidingMenuBarDockBottom) {
        
        int indexButton = 0;
        CGSize contentSize = CGSizeMake(buttonList.count * SlidingWidthButton, SlidingHeightButton);
        [scrollViewContent setContentSize:contentSize];
        for (UIButton *curButton in buttonList) {
            CGRect buttonFrame = curButton.frame;
            buttonFrame.origin.x = indexButton * SlidingHeightButton;
            [curButton setFrame:buttonFrame];
            [scrollViewContent addSubview:curButton];
            indexButton ++;
        }
    } else { //Default is dock Right
        
        int indexButton = 0;
        int moreHeightForPanic = 0;
        for (UIButton *curButton in buttonList) {
            
            CGRect buttonFrame = curButton.frame;
            buttonFrame.origin.y = indexButton * SlidingHeightButton + moreHeightForPanic;
            if(curButton.tag == tagOfPanicButton) {
                moreHeightForPanic = abs(SlidingWidthButton - SlidingHeightButton);
                if(buttonFrame.size.height > buttonFrame.size.width){
                    buttonFrame.size.width = buttonFrame.size.height;
                } else {
                    buttonFrame.size.height = buttonFrame.size.width;
                }
            }
            [curButton setFrame:buttonFrame];
            [scrollViewContent addSubview:curButton];
            CGSize contentSize = CGSizeMake(SlidingWidthButton, buttonList.count * SlidingHeightButton + moreHeightForPanic);
            [scrollViewContent setContentSize:contentSize];
            indexButton ++;
        }
        
        [self scrollViewDidScroll:scrollViewContent];
    }
}

//Remove all menu buttons
-(void)removeAllButtons
{
    for (UIButton *curButton in buttonList) {
        [curButton removeFromSuperview];
    }
    [buttonList removeAllObjects];
    
    //Hide highlight images
    UIImageView *imageRemove = nil;
    imageRemove = (UIImageView*)[self.view viewWithTag:tagTopHighlight];
    [imageRemove setHidden:YES];
    imageRemove = (UIImageView*)[self.view viewWithTag:tagBottomHighlight];
    [imageRemove setHidden:YES];
    imageRemove = (UIImageView*)[self.view viewWithTag:tagLeftHighlight];
    [imageRemove setHidden:YES];
    imageRemove = (UIImageView*)[self.view viewWithTag:tagRightHighlight];
    [imageRemove setHidden:YES];
}

-(void)hideAll
{
    [self.view setHidden:YES];
    [movingButton setHidden:YES];
}

-(void)showAll
{
    [self.view setHidden:NO];
    [movingButton setHidden:NO];
    if (_slidingType == SlidingType_Menu) {
        [movingButton setTitle:[L_btn_home_swipe currentKeyToLanguage] forState:UIControlStateNormal];
    } else { //Legend
        [movingButton setTitle:[L_btn_legend_swipe currentKeyToLanguage] forState:UIControlStateNormal];
    }
    [self assignCaptionTextForLegend];
}

-(void)removeButtonAtIndex:(int)indexRemove
{
    NSMutableArray *listRemove = [NSMutableArray array];
    [listRemove addObject:[buttonList objectAtIndex:indexRemove]];
    [buttonList removeObjectsInArray:listRemove];
    
    //repaint all button after remove
    [self paintAllButtons];
}

//Set caption for menu sync on pressed or sync completed
-(void)setSyncingStatusButton:(BOOL)isSyncing
{
    UIButton *syncButton = (UIButton*) [scrollViewContent viewWithTag:tagOfSyncNowButton];
    
    if(isSyncing){
        [syncButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_SYNC_NOW] forState:UIControlStateNormal];
    } else {
        [syncButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_MSG_SYNC_RUNNING_MESSAGE] forState:UIControlStateNormal];
    }
    
}

-(void) show:(BOOL) isAnimated
{
//    if(!isShowing){
        isShowing = YES;
        
        if (dockType == SlidingMenuBarDockTop) {
            CGRect rectView = self.view.frame;
            rectView.origin.y = yMenuBarShow;
            CGRect movingButtonFrame = movingButton.frame;
            movingButtonFrame.origin.y = yMenuBarShow + rectView.size.height;
            if(isAnimated){
                [UIView animateWithDuration:0.25f
                                 animations:^{
                                     [self.view setFrame:rectView];
                                     [movingButton setFrame:movingButtonFrame];
                                 }];
            } else {
                [self.view setFrame:rectView];
                [movingButton setFrame:movingButtonFrame];
            }
        } else if (dockType == SlidingMenuBarDockBottom) {
            CGRect rectView = self.view.frame;
            rectView.origin.y = yMenuBarShow;
            CGRect movingButtonFrame = movingButton.frame;
            movingButtonFrame.origin.y = yMenuBarShow - movingButtonFrame.size.height;
            if(isAnimated) {
                [UIView animateWithDuration:0.25f
                                 animations:^{
                                     [self.view setFrame:rectView];
                                     [movingButton setFrame:movingButtonFrame];
                                 }];
            } else {
                [self.view setFrame:rectView];
                [movingButton setFrame:movingButtonFrame];
            }
        } else if (dockType == SlidingMenuBarDockLeft) {
            CGRect rectView = self.view.frame;
            rectView.origin.x = xMenuBarShow;
            CGRect movingButtonFrame = movingButton.frame;
            movingButtonFrame.origin.x = xMenuBarShow + rectView.size.width;
            if(isAnimated) {
                [UIView animateWithDuration:0.25f
                                 animations:^{
                                     [self.view setFrame:rectView];
                                     [movingButton setFrame:movingButtonFrame];
                                 }];
            } else {
                [self.view setFrame:rectView];
                [movingButton setFrame:movingButtonFrame];
            }
        } else {
            CGRect rectView = self.view.frame;
            rectView.origin.x = xMenuBarShow;
            CGRect movingButtonFrame = movingButton.frame;
            movingButtonFrame.origin.x = xMenuBarShow - movingButtonFrame.size.width;
            if(isAnimated) {
                [UIView animateWithDuration:0.2f
                                 animations:^{
                                     [self.view setFrame:rectView];
                                     [movingButton setFrame:movingButtonFrame];
                                 }];
            } else {
                [self.view setFrame:rectView];
                [movingButton setFrame:movingButtonFrame];
            }
        }
//    }
}

-(void)highlightSelectedTag
{
    UIButton *curButton = [self buttonWithTag:currentSelected];
    [selectedImage removeFromSuperview];
    CGRect frameSelectedImage = curButton.frame;
    frameSelectedImage.origin.x = 0;
    frameSelectedImage.origin.y = 0;
    [selectedImage setFrame:frameSelectedImage];
    [curButton insertSubview:selectedImage atIndex:0];
}

-(void)hide:(BOOL) isAnimated
{
//    if(isShowing){
        isShowing = NO;
        
        if (dockType == SlidingMenuBarDockTop) {
            CGRect rectView = self.view.frame;
            rectView.origin.y =  yMenuBarShow - rectView.size.height;
            CGRect movingButtonFrame = movingButton.frame;
            movingButtonFrame.origin.y = 0;
            if(isAnimated){
                [UIView animateWithDuration:0.25f
                                 animations:^{
                                     [self.view setFrame:rectView];
                                     [movingButton setFrame:movingButtonFrame];
                                 }];
            } else {
                [self.view setFrame:rectView];
                [movingButton setFrame:movingButtonFrame];
            }
        } else if (dockType == SlidingMenuBarDockBottom) {
            CGRect rectView = self.view.frame;
            rectView.origin.y = yMenuBarShow + rectView.size.height;
            CGRect movingButtonFrame = movingButton.frame;
            movingButtonFrame.origin.y = yMenuBarShow + rectView.size.height - movingButtonFrame.size.height;
            if(isAnimated) {
                [UIView animateWithDuration:0.25f
                                 animations:^{
                                     [self.view setFrame:rectView];
                                     [movingButton setFrame:movingButtonFrame];
                                 }];
            } else {
                [self.view setFrame:rectView];
                [movingButton setFrame:movingButtonFrame];
            }
        } else if (dockType == SlidingMenuBarDockLeft) {
            CGRect rectView = self.view.frame;
            rectView.origin.x = xMenuBarShow - rectView.size.width;
            CGRect movingButtonFrame = movingButton.frame;
            movingButtonFrame.origin.x = 0;
            if(isAnimated) {
                [UIView animateWithDuration:0.25f
                                 animations:^{
                                     [self.view setFrame:rectView];
                                     [movingButton setFrame:movingButtonFrame];
                                 }];
            } else {
                [self.view setFrame:rectView];
                [movingButton setFrame:movingButtonFrame];
            }
        } else {
            CGRect rectView = self.view.frame;
            rectView.origin.x = xMenuBarShow + rectView.size.width;
            CGRect movingButtonFrame = movingButton.frame;
            movingButtonFrame.origin.x = xMenuBarShow + rectView.size.width - movingButtonFrame.size.width;
            if(isAnimated) {
                [UIView animateWithDuration:0.2f
                                 animations:^{
                                     [self.view setFrame:rectView];
                                     [movingButton setFrame:movingButtonFrame];
                                 }];
            } else {
                [self.view setFrame:rectView];
                [movingButton setFrame:movingButtonFrame];
            }
        }
//    }
}

-(void)setAlwaysHoldPanicButton:(BOOL)isHolding
{
    UIButton *panicButton = nil;
    //Finding Panic Button
    for (UIButton *curButton in buttonList) {
        if(curButton.tag == tagOfPanicButton) {
            panicButton = curButton;
        }
    }
    
    if(panicButton){
        if(isHolding) {
            [panicButton setBackgroundImage:[UIImage imageNamed:imgPanicPressed] forState:UIControlStateNormal];
        } else {
            [panicButton setBackgroundImage:[UIImage imageNamed:imgPanic] forState:UIControlStateNormal];
        }
    }
}

//MARK: LEGENDS
-(void)setupLegend
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [_imgLegendPending setImage:[UIImage imageNamed:imgPendingFlat]];
        [_imgLegendReassignPending setImage:[UIImage imageNamed:@"legend_reassign_2"]];
        [_imgLegendDND setImage:[UIImage imageNamed:imgDNDFlat]];
        [_imgLegendDoubleLock setImage:[UIImage imageNamed:imgDoubleLockFlat]];
        [_imgLegendServiceLater setImage:[UIImage imageNamed:imgServiceLaterFlat]];
        [_imgLegendDeclinedService setImage:[UIImage imageNamed:imgDeclinedServiceFlat]];
        [_imgLegendStart setImage:[UIImage imageNamed:imgStartFlat]];
        [_imgLegendStop setImage:[UIImage imageNamed:imgStopFlat]];
        [_imgLegendComplete setImage:[UIImage imageNamed:imgCompleteFlat]];
        [_imgLegendQueue setImage:[UIImage imageNamed:imgQueueRoomFlat]];
        [_imgLegendPause setImage:[UIImage imageNamed:@"icon_pause_2"]];
        [_imgLegendRush setImage:[UIImage imageNamed:imgRushRoomFlat]];
        [_imgLegendReassignComplete setImage:[UIImage imageNamed:@"legend_reassign_complete_2"]];
        [_imgLegendMakeUpReassign setImage:[UIImage imageNamed:@"legend_makeup_reassign_2"]];
        [_imgLegendReleaseOOS setImage:[UIImage imageNamed:@"legend_release_oos_2"]];
        [_imgLegendStayOver setImage:[UIImage imageNamed:@"legend_stay_over_2"]];
        [_imgLegendProfileNote setImage:[UIImage imageNamed:@"legend_profile_note_2"]];
    }
}

//MARK: Private Functions

-(void)swipeLeftDetected:(UIGestureRecognizer*)gestureRecognize
{
    if(dockType == SlidingMenuBarDockLeft) {
        [self hide:YES];
    } else if(dockType == SlidingMenuBarDockRight){
        [self show:YES];
    }
}

-(void)swipeRightDetected:(UIGestureRecognizer*)gestureRecognize
{
    if(dockType == SlidingMenuBarDockLeft) {
        [self show:YES];
        
    } else if(dockType == SlidingMenuBarDockRight) {
        [self hide:YES];
    }
}

-(void)swipeUpDetected:(UIGestureRecognizer*)gestureRecognize
{
    if(dockType == SlidingMenuBarDockTop) {
        [self hide:YES];
    } else if(dockType == SlidingMenuBarDockBottom) {
        [self show:YES];
    }
}

-(void)swipeDownDetected:(UIGestureRecognizer*)gestureRecognize
{
    if(dockType == SlidingMenuBarDockTop) {
        [self show:YES];
        
    } else if(dockType == SlidingMenuBarDockBottom) {
        [self hide:YES];
    }
}

-(UIButton*)insertMovingButton
{
    movingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [movingButton setTag:TagMovingButton];
    [movingButton setUserInteractionEnabled:YES];
    //[movingButton addTarget:self action:@selector(didTappedMovingButton:) forControlEvents:UIControlEventTouchDown];
    //[movingButton setTitleEdgeInsets:UIEdgeInsetsMake(SlidingHeightButton/2 + 9, 0, 0, 0)];
    
    //Drag drop detector
    UIPanGestureRecognizer *dragDropSlidingMenu = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panMovingButtonDetected:)];
    [movingButton addGestureRecognizer:dragDropSlidingMenu];
    
    UIFont *buttabfont = [UIFont boldSystemFontOfSize:14];
    [movingButton setTitleColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f] forState:UIControlStateNormal];
    [movingButton.titleLabel setFont:buttabfont];
    [movingButton setBackgroundColor:MovingBackgroundButton];
    
    if (_slidingType == SlidingType_Menu) {
        [movingButton setTitle:[L_btn_home_swipe currentKeyToLanguage] forState:UIControlStateNormal];
    } else { //Legend
        [movingButton setTitle:[L_btn_legend_swipe currentKeyToLanguage] forState:UIControlStateNormal];
    }
    
    [movingButton addTarget:self action:@selector(didTappedMovingButton:) forControlEvents:UIControlEventTouchUpInside];
    
    if(dockType == SlidingMenuBarDockRight) {
        movingButton.transform = CGAffineTransformMakeRotation(-M_PI/2);
        CGRect frameButton = self.view.frame;
        
        //Change this for change button size
        int widthMoving = 25;
        int heightMoving = 65;
        
        frameButton.origin.x -= widthMoving;
        frameButton.origin.y = (frameButton.origin.y + frameButton.size.height)/2 - heightMoving/5;
        frameButton.size.width = widthMoving;
        frameButton.size.height = heightMoving;
        [movingButton setFrame:frameButton];
        
        [self setMaskTo:movingButton byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
        [self.view.superview addSubview:movingButton];
    } else if (dockType == SlidingMenuBarDockLeft) {
        
        movingButton.transform = CGAffineTransformMakeRotation(M_PI/2);
        CGRect frameButton = self.view.frame;
        
        //Change this for change button size
        int widthMoving = 25;
        int heightMoving = 65;
        
        frameButton.origin.x = 0;
        if (_slidingType == SlidingType_Menu) {
            frameButton.origin.y = (frameButton.origin.y + frameButton.size.height)/2 - heightMoving/5;
        } else { //LEGEND
            frameButton.origin.y = frameButton.size.height - heightMoving/5 - 150;
        }
        frameButton.size.width = widthMoving;
        frameButton.size.height = heightMoving;
        [movingButton setFrame:frameButton];
        
        [self setMaskTo:movingButton byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
        [self.view.superview addSubview:movingButton];
    }
    
    return movingButton;
}

-(void) insertHighlightImages
{
    if(dockType == SlidingMenuBarDockRight) {
        UIImageView *imageTopHighlight = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgMenuTop]];
        [imageTopHighlight setTag:tagTopHighlight];
        imageTopHighlight.frame = CGRectMake(0, 0, SlidingWidthButton, 5);
        [imageTopHighlight setHidden:YES];
        [self.view addSubview:imageTopHighlight];
        
        UIImageView *imageBottomHighlight = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgMenuBottom]];
        [imageBottomHighlight setTag:tagBottomHighlight];
        imageBottomHighlight.frame = CGRectMake(0, self.view.frame.size.height - 5, SlidingWidthButton, 5);
        [imageBottomHighlight setHidden:YES];
        [self.view addSubview:imageBottomHighlight];
    
    } else if(dockType == SlidingMenuBarDockLeft) {
        UIImageView *imageTopHighlight = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgMenuTop]];
        [imageTopHighlight setTag:tagTopHighlight];
        imageTopHighlight.frame = CGRectMake(0, 0, SlidingWidthButton, 5);
        [imageTopHighlight setHidden:YES];
        [self.view addSubview:imageTopHighlight];
        
        UIImageView *imageBottomHighlight = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgMenuBottom]];
        [imageBottomHighlight setTag:tagBottomHighlight];
        imageBottomHighlight.frame = CGRectMake(0, self.view.frame.size.height - 5, SlidingWidthButton, 5);
        [imageBottomHighlight setHidden:YES];
        [self.view addSubview:imageBottomHighlight];
    }
}

-(void) setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners
{
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(8.0, 8.0)];
    
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    
    CAShapeLayer * strokeLayer = [CAShapeLayer layer];
    strokeLayer.path = rounded.CGPath;
    strokeLayer.strokeColor = SlidingBackgroundColor.CGColor;
    strokeLayer.fillColor = [UIColor clearColor].CGColor;
    [strokeLayer setLineWidth:3.0f];
    
    [view.layer addSublayer:strokeLayer];
    view.layer.mask = shape;
    
}
-(void)dockViewWithType:(enum SlidingMenuBarDock) dockTypeValue
{
    CGRect scrollViewContentFrame = scrollViewContent.frame;
    dockType = dockTypeValue;
    if(dockType == SlidingMenuBarDockTop) {
        scrollViewContentFrame.origin.x = 0;
        yMenuBarShow = 0;
        scrollViewContentFrame.origin.y = yMenuBarShow;
        scrollViewContentFrame.size.width = superViewFrame.size.width;
        scrollViewContentFrame.size.height = SlidingHeightButton;
        [self.view setFrame:scrollViewContentFrame];
        scrollViewContentFrame.origin.x = 0;
        scrollViewContentFrame.origin.y = 0;
        [scrollViewContent setFrame:scrollViewContentFrame];
        [self.view addSubview:scrollViewContent];
        
    } else if(dockType == SlidingMenuBarDockBottom) {
        scrollViewContentFrame.origin.x = 0;
        yMenuBarShow = superViewFrame.origin.y + superViewFrame.size.height - SlidingHeightButton;
        scrollViewContentFrame.origin.y = yMenuBarShow;
        scrollViewContentFrame.size.width = superViewFrame.size.width;
        scrollViewContentFrame.size.height = SlidingHeightButton;
        [self.view setFrame:scrollViewContentFrame];
        scrollViewContentFrame.origin.x = 0;
        scrollViewContentFrame.origin.y = 0;
        [scrollViewContent setFrame:scrollViewContentFrame];
        [self.view addSubview:scrollViewContent];
        
    } else if(dockType == SlidingMenuBarDockLeft){
        
        xMenuBarShow = 0;
        scrollViewContentFrame.origin.x = xMenuBarShow;
        if (_slidingType == SlidingType_Menu ) {
            scrollViewContentFrame.origin.y = HEIGHT_NAVIGATION_BAR;
            scrollViewContentFrame.size.width = SlidingWidthButton;
            scrollViewContentFrame.size.height = superViewFrame.size.height - HEIGHT_NAVIGATION_BAR;
        } else { //Legend
            scrollViewContentFrame.origin.y = 0;
            scrollViewContentFrame.size.width = SlidingWidthScrollViewLegend;
            scrollViewContentFrame.size.height = superViewFrame.size.height;
        }
        [self.view setFrame:scrollViewContentFrame];
        scrollViewContentFrame.origin.x = 0;
        scrollViewContentFrame.origin.y = 0;
        [scrollViewContent setFrame:scrollViewContentFrame];
        [self.view addSubview:scrollViewContent];
        
    } else { //Default is Dock Right
        dockType = SlidingMenuBarDockRight;
        
        xMenuBarShow = superViewFrame.origin.x + superViewFrame.size.width - SlidingWidthButton;
        scrollViewContentFrame.origin.x = xMenuBarShow;
        int heightNavigationBar = HEIGHT_NAVIGATION_BAR;
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            heightNavigationBar += 20;
        }
  
        scrollViewContentFrame.origin.y = heightNavigationBar;
        scrollViewContentFrame.size.width = SlidingWidthButton;
        scrollViewContentFrame.size.height = superViewFrame.size.height - heightNavigationBar;
        [self.view setFrame:scrollViewContentFrame];
        scrollViewContentFrame.origin.x = 0;
        scrollViewContentFrame.origin.y = 0;
        [scrollViewContent setFrame:scrollViewContentFrame];
        [self.view addSubview:scrollViewContent];
    }
}

-(UIButton*)buttonWithTag:(int)tagButtonMenu
{
    return (UIButton*)[scrollViewContent viewWithTag:tagButtonMenu];
}


-(void)didTappedMovingButton:(UIButton*)movingButton
{
    if(isShowing) {
        [self hide:YES];
    } else {
        [self show:YES];
    }
}

-(void)didChoseButton:(UIButton*)buttonSelected
{
    [self hide:YES];
    //boundView.origin.x = 240;
    if([delegate respondsToSelector:@selector(didSelectedButtonIndex:tag:)]) {
        
        int index = 0;
        for (UIButton *curButton in buttonList) {
            if(curButton.tag == buttonSelected.tag) {
                if([delegate respondsToSelector:@selector(didSelectedButtonIndex:tag:)]){
                    [delegate didSelectedButtonIndex:index tag:(int)buttonSelected.tag];
                }
                index ++;
                break;
            }
        }
    }
}

//Detect drag and drop moving button
-(void)panMovingButtonDetected:(UIPanGestureRecognizer*)recognizer
{
    CGPoint translation = [recognizer translationInView:self.view];
    
    if (dockType == SlidingMenuBarDockTop) {
        
    } else if(dockType == SlidingMenuBarDockBottom) {
        
    } else if(dockType == SlidingMenuBarDockLeft) {
        
        //Checking drop the button Moving
        if(recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
            
            CGPoint velocity = [recognizer velocityInView:self.view];
            if(velocity.x > 0) {
                [self show:YES];
            } else if(velocity.x <= 0) {
                [self hide:YES];
            } else if((self.view.frame.origin.x >= xMenuBarShow - self.view.frame.size.width / 2)) {
                [self show:YES];
            } else {
                [self hide:YES];
            }
        } else {
            
            //CGRect frameSlidingMenuBar = self.view.frame;
            CGRect buttonMovingFrame = movingButton.frame;
            
            //Checking moving
            buttonMovingFrame.origin.x += translation.x;
            if(buttonMovingFrame.origin.x <= 0) { //Reach min of x position frame showed
                [self hide:NO];
            } else if (buttonMovingFrame.origin.x >= self.view.frame.size.width) { //Reach max of x position frame hidden
                [self show:NO];
            } else {
                recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x, recognizer.view.center.y);
                self.view.center = CGPointMake(self.view.center.x + translation.x, self.view.center.y);
            }
        }
        
    } else { //Dock Right
        
        //Checking drop the button Moving
        if(recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
            
            CGPoint velocity = [recognizer velocityInView:self.view];
            if(velocity.x < 0) {
                [self show:YES];
            } else if(velocity.x > 0) {
                [self hide:YES];
            } else if((self.view.frame.origin.x < xMenuBarShow + self.view.frame.size.width / 2)) {
                [self show:YES];
            } else {
                [self hide:YES];
            }
        } else {
            
            //CGRect frameSlidingMenuBar = self.view.frame;
            CGRect buttonMovingFrame = movingButton.frame;
            
            //Checking moving
            buttonMovingFrame.origin.x += translation.x;
            if(buttonMovingFrame.origin.x < xMenuBarShow - buttonMovingFrame.size.width) { //Reach min of x position frame showed
                [self show:NO];
            } else if (buttonMovingFrame.origin.x > xMenuBarShow + self.view.frame.size.width - buttonMovingFrame.size.width) { //Reach max of x position frame hidden
                [self hide:NO];
            } else {
                recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x, recognizer.view.center.y);
                self.view.center = CGPointMake(self.view.center.x + translation.x, self.view.center.y);
            }
        }
    }
    
    //Reset translation for another pan
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(dockType == SlidingMenuBarDockRight) {
        UIImageView *imgTopHighlight = (UIImageView *)[self.view viewWithTag:tagTopHighlight];
        imgTopHighlight.hidden = YES;
        
        UIImageView *imgBottomHighlight = (UIImageView *)[self.view viewWithTag:tagBottomHighlight];
        imgBottomHighlight.hidden = YES;
        
        if(scrollView.contentOffset.y > 0.0)
        {
            //imgTopHighlight.frame = CGRectMake(0, 0, SlidingWidthButton, 5);
            imgTopHighlight.hidden = NO;
        }
        
        if((scrollView.contentOffset.y + scrollView.frame.size.height) <  scrollView.contentSize.height /*buttonList.count * SlidingHeightButton*/)
        {
            //imgBottomHighlight.frame = CGRectMake(0, self.view.frame.size.height - 5, SlidingWidthButton, 5);
            imgBottomHighlight.hidden = NO;
        }
    }
}

//Set title menu button with current language
-(UIButton*)reloadTitleButton:(UIButton*)butTabBar
{
    [movingButton setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_HOME] forState:UIControlStateNormal];
    
    int tagButtonMenu = (int)butTabBar.tag;
    if(tagButtonMenu == tagOfHomeButton) {
        
        [butTabBar setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_HOME] forState:UIControlStateNormal];
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuHomeFlat] forState:UIControlStateNormal];
        } else {
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuHome] forState:UIControlStateNormal];
        }
        
    } else if(tagButtonMenu == tagOfAdditionalJob) {
        
        [butTabBar setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_JOB] forState:UIControlStateNormal];
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuJobFlat] forState:UIControlStateNormal];
        } else {
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuJob] forState:UIControlStateNormal];
        }
    } else if(tagButtonMenu == tagOfFindButton) {
        
        [butTabBar setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_FIND] forState:UIControlStateNormal];
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuFindFlat] forState:UIControlStateNormal];
        } else {
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuFind] forState:UIControlStateNormal];
        }
    } else if(tagButtonMenu == tagOfHistoryButton) {
        
        [butTabBar setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_HISTORY_BOTTOM_TABAR] forState:UIControlStateNormal];
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuHistoryFlat] forState:UIControlStateNormal];
        } else {
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuHistory] forState:UIControlStateNormal];
        }
    } else if(tagButtonMenu == tagOfLogOutButton) {
        
        [butTabBar setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_LOGOUT] forState:UIControlStateNormal];
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuLogoutFlat] forState:UIControlStateNormal];
        } else {
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuLogout] forState:UIControlStateNormal];
        }
        
    } else if(tagButtonMenu == tagOfMessageButton) {
        
        [butTabBar setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_MESSAGE] forState:UIControlStateNormal];
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuMessageFlat] forState:UIControlStateNormal];
        } else {
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuMessage] forState:UIControlStateNormal];
        }
    } else if(tagButtonMenu == tagOfPTT) {
        
        [butTabBar setTitle:@"m-2Talk" forState:UIControlStateNormal];
        [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuPTT] forState:UIControlStateNormal];
        
    } else if(tagButtonMenu == tagOfSettingButton) {
        
        [butTabBar setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_SETTING] forState:UIControlStateNormal];
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuSettingFlat] forState:UIControlStateNormal];
        } else {
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuSetting] forState:UIControlStateNormal];
        }
        
    } else if(tagButtonMenu == tagOfSyncNowButton) {
        
        [butTabBar setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_SYNC_NOW] forState:UIControlStateNormal];
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuSyncFlat] forState:UIControlStateNormal];
        } else {
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuSync] forState:UIControlStateNormal];
        }
        
    } else if(tagButtonMenu == tagOfUnassignButton) {
        
        [butTabBar setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_UNASSIGN] forState:UIControlStateNormal];
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuUnassignFlat] forState:UIControlStateNormal];
        } else {
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuUnassign] forState:UIControlStateNormal];
        }
        
    } else if (tagButtonMenu == tagOfPostingButton){
        
        [butTabBar setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_BTN_POSTING] forState:UIControlStateNormal];
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuPostingFlat] forState:UIControlStateNormal];
        } else {
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuPosting] forState:UIControlStateNormal];
        }
    } else if (tagButtonMenu == tagOfPanicButton) {
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgPanic] forState:UIControlStateNormal];
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgPanicPressed] forState:UIControlEventTouchDown];
        } else {
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgPanic] forState:UIControlStateNormal];
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgPanicPressed] forState:UIControlEventTouchDown];
        }
    } else if (tagButtonMenu == tagOfGuestProfileButton) {
        
        [butTabBar setTitle:[[LanguageManagerV2 sharedLanguageManager] getStringLanguageByStringId:L_menu_guest_profile] forState:UIControlStateNormal];
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuGuestInfo] forState:UIControlStateNormal];
        } else {
            [butTabBar setBackgroundImage:[UIImage imageNamed:imgMenuGuestInfo] forState:UIControlStateNormal];
        }
    }
    
    return butTabBar;
}

- (void)reloadAllLegendDescriptions {
    
    [movingButton setTitle:[L_btn_legend_swipe currentKeyToLanguage] forState:UIControlStateNormal];
    [self assignCaptionTextForLegend];
}

- (void)assignCaptionTextForLegend {
    [_lblTitleLegend setText:[L_btn_legend_swipe currentKeyToLanguage]];
    [_lblLegendPending setText:[L_legend_pending currentKeyToLanguage]];
    [_lblLegendReassignPending setText:[L_legend_reassign_pending currentKeyToLanguage]];
    [_lblLegendReassignComplete setText:[L_legend_reassign_complete currentKeyToLanguage]];
    [_lblLegendMakeUpReassign setText:[L_legend_make_up_room_reassign currentKeyToLanguage]];
    [_lblLegendDND setText:[L_legend_dnd currentKeyToLanguage]];
    [_lblLegendDoubleLock setText:[L_legend_double_lock currentKeyToLanguage]];
    [_lblLegendServiceLater setText:[L_legend_service_later currentKeyToLanguage]];
    [_lblLegendDeclinedService setText:[L_legend_declined_service currentKeyToLanguage]];
    [_lblLegendStart setText:[L_legend_start currentKeyToLanguage]];
    [_lblLegendPause setText:[L_legend_pause currentKeyToLanguage]];
    [_lblLegendStop setText:[L_legend_stop currentKeyToLanguage]];
    [_lblLegendComplete setText:[L_legend_completed currentKeyToLanguage]];
    [_lblLegendRush setText:[L_legend_rush_room currentKeyToLanguage]];
    [_lblLegendQueue setText:[L_legend_queue_room currentKeyToLanguage]];
    [_lblLegendReleaseOOS setText:[L_legend_out_of_service currentKeyToLanguage]];
    [_lblLegendProfileNote setText:[L_legend_profile_notes currentKeyToLanguage]];
    [_lblLegendStayOver setText:[L_legend_stayover currentKeyToLanguage]];
    [_lblLegendPreviousDay setText:[L_legend_privious_day currentKeyToLanguage]];
    [_lblLegendDueIn setText:[L_legend_due_in currentKeyToLanguage]];
    [_lblLegendDueOut setText:[L_legend_due_out currentKeyToLanguage]];
}

@end
